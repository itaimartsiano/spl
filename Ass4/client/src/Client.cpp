#include "../include/Client.h"
#include "../include/ConnectionHandler.h"
#include "../include/urlencode.h"
#include <stdlib.h>
#include <boost/locale.hpp>
#include <iostream>
#include <string>
#include <boost/thread.hpp>


using namespace std;
using namespace boost;
class urlencode;



Task::Task(bool& Exit, ConnectionHandler & connectionHandler1):
				logout_(false), cookie_(""), Exit_(Exit), connectionHandler_(connectionHandler1), lockObject_(){
}


Task::~Task(){

}



//handle user commands
void Task::getUserCommands(){

	while (!(logout_) && !(Exit_)){

		if (cookie_.length()==0){
			lockObject_.lock();             //until we receive a cookie from server we can't ask for messages this lock ensure that the other thread won't retrieve messages
		}

		cout << "Write command to server: ";
		string messageFromUser = "";
		getline(cin,messageFromUser); //Receiving a message from the user

		if (cookie_.length()>0){
				lockObject_.lock();     // we received a commend from client and now we need communicaton with the server this lock  ensure that the other thread won't interfere with the communication
		}

		messageFromUser = handleCommand(messageFromUser);

		if (messageFromUser.length() > 0){       //checking if the message from the user is ok
			connectionHandler_.sendLine(messageFromUser);     //sending the message to the server
			string messageFromServer;
			connectionHandler_.getLine(messageFromServer);   // receiving a response from the server
			messageFromServer = handleMessageFromServer(messageFromServer);
			cout << messageFromServer << endl;

		}
		else cout << "missing parameters ,no commend was done." << endl;  //message not ok ,informing the user

		lockObject_.unlock();

	}

}



//handle messages from server

string Task::handleMessageFromServer(string messagFromServer){

	string messageToUser="";

	if (messagFromServer.find("200") != string::npos){  //check if the response from the server not an error

		if (cookie_.length()==0){
			cookie_ = messagFromServer.substr(messagFromServer.find('u'), messagFromServer.find("\n\n")-messagFromServer.find('u'));  //set cookie
		}
		messageToUser = messagFromServer.substr(messagFromServer.find("\n\n")+1,messagFromServer.length()-3-messagFromServer.find("\n\n"));  //set the message to the user
		}
	else {

		messageToUser = "Error: " +  messagFromServer.substr(messagFromServer.find("\n\n\n")+3,messagFromServer.length()-5-messagFromServer.find("\n\n\n"));

	}
	return messageToUser;

}


//getting messages from connection handler
void Task::retreiveMessages(){

	while (!(logout_) && !(Exit_)){

		this_thread::sleep( boost::posix_time::seconds(2) );        

		lockObject_.lock();      //we synchronize on this object so that the thread that responsible on the commends from the client won't interfere withe the communication with the server

		string messagesRequest = "GET /queue.jsp HTTP/1.1\nCookie: "+ cookie_ + "\n\n\n";

		if (!(logout_) && !(Exit_)){
			connectionHandler_.sendLine(messagesRequest);     //sending the request to the server
			string messageFromServer;
			connectionHandler_.getLine(messageFromServer);   // receiving a response from the server
			messageFromServer = handleMessageFromServer(messageFromServer);

			lockObject_.unlock();   // if the other thread need to print his response /communicate with the server

			lockObject_.lock();  // print the messages on the screen this lock ensure that the other thread won't interfere

			if (messageFromServer.find("No new messages") == string::npos ){  //checking if there are messages to print
				cout <<messageFromServer << endl;
			}
		}
		lockObject_.unlock();
	}


}



string Task::handleCommand(string commend){

	string ans;
	 int Break = commend.find_first_of(' ');

	 string theCommend = commend.substr(0,Break);
	 if (theCommend.compare("Logout") == 0) { ans = logoutMsg(cookie_);}
	 	 else{
	 		 commend = commend.substr(Break+1);    //leaving only the parameters
	 		 if (theCommend.compare("Login") == 0) { ans = loginMsg(commend);}
	 		 else{
	 			 if (theCommend.compare("List") == 0) { ans = listMsg(commend,cookie_);}
	 			 else{
	 				 if (theCommend.compare("Send") == 0) { ans = sendMsg(commend,cookie_);}
	 				 else{
	 					 if (theCommend.compare("Add") == 0){ ans = addMsg(commend,cookie_);}
	 					 else{
	 						 if (theCommend.compare("Remove") == 0){ ans = removeMsg(commend,cookie_);}
	 						 else {
	 							 if (theCommend.compare("CreateGroup") == 0){ ans = createGroupMsg(commend,cookie_);}
	 							 else{
	 								 if (theCommend.compare("Exit") == 0){ ans = exitMsg(cookie_);}
	 							 }
	 						 }
	 					 }
	 				 }
	 			 }
	 		 }
	 	 }
	 return ans;
}




string Task::logoutMsg(string cookie){

	string ans;
	ans.append("GET /logout.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\n\n");
	logout_ = true;
	return ans;

}



string Task::loginMsg(string commend){

	string ans;
	ans.append("POST /login.jsp HTTP/1.1");
	ans.append("\n\nUserName=");
	string userName = commend.substr(0,commend.find(' '));
	ans.append(url_encode(userName));
	int Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&Phone=");
	ans.append(url_encode(commend));
	ans.append("\n");

	return ans;

}



string Task::listMsg(string commend,string cookie){

	string ans;
	ans.append("POST /list.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\nList=");
	ans.append(commend);

	if ((commend.compare("Groups")!=0)&&(commend.compare("Users")!=0)){
		int Break = commend.find_first_of(' ');
		commend = commend.substr(Break+1);
		ans.append("&Group=");
		ans.append(commend);
	}
	ans.append("\n");

	return ans;
}



string Task::sendMsg(string commend,string cookie){

	string ans;
	ans.append("POST /send.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\nType=");
	string msgType = commend.substr(0,commend.find(' '));

	if (msgType == "User"){
		ans.append(url_encode("Direct"));
	}
	else ans.append(url_encode("Group"));

	int Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&Target=");
	string msgTarget = commend.substr(0,commend.find(' '));
	ans.append(url_encode(msgTarget));
	Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&Content=");
	ans.append(url_encode(commend));
	ans.append("\n");

	return ans;
}



string Task::addMsg(string commend,string cookie){

	string ans;
	ans.append("POST /add_user.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\nTarget=");
	string userName = commend.substr(0,commend.find(' '));
	ans.append(url_encode(userName));
	int Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&User=");
	ans.append(url_encode(commend));
	ans.append("\n");

	return ans;
}



string Task::removeMsg(string commend,string cookie){

	string ans;
	ans.append("POST /remove_user.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\nTarget=");
	string target = commend.substr(0,commend.find(' '));
	ans.append(url_encode(target));
	int Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&User=");
	ans.append(url_encode(commend));
	ans.append("\n");

	return ans;
}



string Task::createGroupMsg(string commend,string cookie){

	string ans;
	ans.append("POST /create_group.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\nGroupName=");
	string groupName = commend.substr(0,commend.find(' '));
	ans.append(url_encode(groupName));
	int Break = commend.find_first_of(' ');
	commend = commend.substr(Break+1);
	ans.append("&Users=");
	ans.append(url_encode(commend));
	ans.append("\n");

	return ans;
}



string Task::exitMsg(string cookie){

	string ans;
	ans.append("GET /exit.jsp HTTP/1.1");
	ans.append("\nCookie: ");
	ans.append(cookie);
	ans.append("\n\n");
	Exit_ = true;

	return ans;
}


Client::Client(){

}

Client::~Client(){

}



void Client::start (string host, string stringPort){

	bool Exit = false;
	short port = atoi(stringPort.c_str());

	while(!Exit){

		ConnectionHandler connectionHandler (host, port);
		if (!connectionHandler.connect()) {
			std::cerr << "Cannot connect to " << host << ":" << port << std::endl;
			break;
		}

		Task task1(Exit, connectionHandler);
		thread th1(&Task::retreiveMessages, &task1);
		thread th2(&Task::getUserCommands, &task1);
		th1.join();
		th2.join();

	}

}



