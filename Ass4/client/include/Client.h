/*
 * Client.h
 *
 *  Created on: Jan 17, 2015
 *      Author: itai
 */

#ifndef CLIENT_H_
#define CLIENT_H_
#include <boost/thread/mutex.hpp>
#include <iostream>
#include <string>
#include "../include/ConnectionHandler.h"
#include "../include/urlencode.h"

using namespace std;
using namespace boost;
class ConnectionHandler;


class Task
{
private:

	bool logout_;
	string cookie_;
	bool& Exit_;
	ConnectionHandler & connectionHandler_;
	mutex lockObject_;

	//methods
	string handleCommand(string command);
	string handleMessageFromServer(string messagFromServer);
	string logoutMsg(string cookie);
	string loginMsg(string command);
	string listMsg(string command,string cookie);
	string sendMsg(string command,string cookie);
	string addMsg(string command,string cookie);
	string removeMsg(string command,string cookie);
	string createGroupMsg(string command,string cookie);
	string exitMsg(string cookie);

	public:
		Task(bool & Exit, ConnectionHandler & connectionHandler);
		virtual ~Task();
		void retreiveMessages();
		void getUserCommands();

};



class Client
{
	public:
		Client();
		virtual ~Client();

		//methods
		void start(const string host,const string port);

};





#endif /* CLIENT_H_ */
