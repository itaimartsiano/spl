package Server;

import java.util.HashMap;
import java.util.UUID;

public class UsersDS {
	
	private HashMap <String, WhatsappUser> _usersByAuthentication;
	private HashMap <String, WhatsappUser> _usersByName;
	private HashMap <String, WhatsappUser> _usersByPhoneNumber;
	private HashMap <String, Group> _groups;
	private Object _authLocker;
	private Object _groupsLocker;
	
	
	public UsersDS(){
		_usersByAuthentication = new HashMap <String,WhatsappUser>();
		_usersByName = new HashMap <String,WhatsappUser>();
		_usersByPhoneNumber = new HashMap <String,WhatsappUser>();
		_groups = new HashMap <String, Group>();
		_authLocker = new Object();
		_groupsLocker = new Object();
	}
	
	
	/**
	 * This method checks if the user has access to resources, ****it assumes the only thread that can delete himself from
	 * the _usersByAuthentucation is himself - so there is no problem with thread safety************ 
	 * @param user_auth
	 * @return
	 */
	public boolean checkAuthentication(String user_auth){
		return _usersByAuthentication.containsKey(user_auth);
	}
	
	
	
	/**
	 * This method return a unique authentication key
	 * @return String
	 */
	public  String getAuthenticationKey(){
		UUID authenticationKey;
		synchronized (_authLocker) {
			authenticationKey = UUID.randomUUID();	
		}
		return (authenticationKey.toString());
	}
	
	
	
	
	/**
	 * This method add a new user to all the data structure
	 * @param user - Whatsapp User
	 */
	public void addUser(WhatsappUser user){
		_usersByAuthentication.put(user.getAuthenticationKey(), user);
		_usersByName.put(user.getUserName(), user);
		_usersByPhoneNumber.put(user.getUserPhoneNumber(), user);
	}
	
	
	
	/**
	 * This method adds an existing user to the data structure sorted by authentication key
	 * @param user -Whatsapp user
	 */
	public void addUserToAuothenticationContainer(WhatsappUser user){
		synchronized (_usersByAuthentication) {
			_usersByAuthentication.put(user.getAuthenticationKey(), user);	
		}
	}
	
	
	
	
	/**
	 * This method checks if the user exist in the data structure
	 * @param name
	 * @return true if the user is in the DS else false
	 */
	public boolean CheckIfUserExist(String name){
		return _usersByName.containsKey(name) ;
	}
	
	
	
	
	/**
	 * This method find and return the Whatsapp user requested by name parameter
	 * @param name - name of the user
	 * @return WhatsappUser
	 */
	public WhatsappUser getUserByName(String name){
		return _usersByName.get(name);
	}
	
	
	
	
	/**
	 * This method find and return  the Whatsapp user requested by phone number parameter
	 * @param PhoneNumber - PhoneNumber of the user
	 * @return WhatsappUser
	 */
	public WhatsappUser getUserByPhoneNumber(String PhoneNumber){
		return _usersByPhoneNumber.get(PhoneNumber) ;
	}
	
	
	
	
	
	/**
	 * This method find and return  the Whatsapp user requested by authentication key parameter
	 * @param authentication key - authentication key of the user
	 * @return WhatsappUser
	 */
	public WhatsappUser getUserByAuthKey(String key){
		return _usersByAuthentication.get(key);
	}
	
	
	
	/**
	 * This method find and return the requested group by name parameter
	 * @param name
	 * @return Group
	 */
	public Group getGroupByName(String name){
		return _groups.get(name);
	}
	
	
	
	/**
	 * This method adds a group to the data structure if it doesn't exist already 
	 * @param name - name of the group
	 * @return null if the group exist in the data structure else return the group
	 */
	public Group addGroup(String name){	
		synchronized (_groupsLocker) {
			if (!(_groups.containsKey(name))){
				Group newGroup = new Group(name);
				_groups.put(name, newGroup);
				return newGroup;
			}else	return null;
		}
	}

	
	
	/**
	 * This method removes the user from the authentication data structure
	 * @param authentication key
	 */
	public void removeUserFromAuothenticationContainer(String _authenticationKey){
		synchronized (_usersByAuthentication) {
			_usersByAuthentication.remove(_authenticationKey);	
		}
	}

	
	
	/**
	 * This method print list of all the users
	 * @return String of all the users separate by "\n"
	 */
	public String printUsersList() {
		StringBuilder sb = new StringBuilder();
		synchronized (_usersByAuthentication) {
			for (WhatsappUser user : _usersByAuthentication.values()){
				sb.append("[").append(user.getUserName()).append("]@[").append(user.getUserPhoneNumber()).append("]\n");
			}
		}
		return sb.toString();
	}

	
	
	
	/**
	 * This method bring a long String with all the groups and the users phone numbers
	 * @return String of the group
	 */
	public String printGroups() {
		StringBuilder sb = new StringBuilder();
		synchronized (_groupsLocker) {
			for (Group group: _groups.values()){	
				sb.append(group.printGroup()).append("\n");	
			}
		}
		return sb.toString();
	}

	
	
	
	/**
	 * This method will return the Users phone number of the asked Group
	 * @param groupName - the asked group name to work on
	 * @return String of phone numbers
	 */
	public String printGroup(String groupName) {
		
		String ans;
		synchronized (_groupsLocker) {
			Group currentGroup = _groups.get(groupName);
			ans = currentGroup.printGroup();
		}
		return ans.substring(ans.indexOf(':')+1);
		
	}

	
	
	
	/**
	 * This method adds a user to a group 
	 * @param group - Group
	 * @param user - WhatsappUser
	 */
	public void addUserToGroup(Group group, WhatsappUser user) {
		group.addUserToGroup(user);
	}

	
	
	/**
	 * This method removes a group from the data structure 
	 * @param group - Group
	 */
	public void removeGroup(Group group) {
		synchronized (_groupsLocker) {
			_groups.remove(group.getGroupName());	
		}
	}

	
	
}
