package Server;

import java.util.ArrayList;

public class WhatsappUser {
	
	private final String _userName;
	private final String _phoneNumber;
	private final String _authenticationKey;
	private ArrayList <String> _containerOfMessages;
	private Object _locker;
	
	
	/**
	 * constructor of whatsapp user
	 * @param userName - the name of user
	 * @param phoneNumber - the user's phone number
	 * @param authenticationKey - the authentication key of the user
	 */
	public WhatsappUser(String userName, String phoneNumber, String authenticationKey) {
		_userName = userName;
		_phoneNumber = phoneNumber;
		_authenticationKey = authenticationKey;
		_containerOfMessages = new ArrayList <String>();
		_locker = new Object();
	}
	
	
	
	
	/**
	 * this method returns the user name
	 * @return field of user name
	 */
	public String getUserName(){
		return _userName;
	}
	
	
	
	
	/**
	 * this method returns the user phone number
	 * @return - field user's phone number
	 */
	public String getUserPhoneNumber(){
		return _phoneNumber;
	}
	
	
	
	
	/**
	 * this method returns the user authentication key
	 * @return - user's field _authenticationkey
	 */
	public String getAuthenticationKey(){
		return _authenticationKey;
	}
	
	
	
	
	/**
	 * this method run over all the arraylist of messages, append the messages to one string 
	 * @return string with a list of messages to user
	 */
	public String getMessages(){
		
		StringBuilder sb = new StringBuilder();
		
		synchronized (_locker) {
			while (_containerOfMessages.iterator().hasNext()){
				sb.append(_containerOfMessages.remove(0)).append("\n");
			}
		}
		
		if (sb.length() == 0){
			sb.append("No new messages\n");
		}
		
		return sb.toString();
		
	}
	
	
	
	
	/**
	 * this method put new message in the container of messages
	 * @param message - the message to enter
	 */
	public void sendMessage(String message){
		
		synchronized (_locker) {
			_containerOfMessages.add(message);
		}
		
	}
	
	
	

}
