package Server;

import java.util.HashMap;

public class Group {
	
	private HashMap <String, WhatsappUser> _containerOfUsers;	
	private final String _groupName;

	public  Group(String name){
		_containerOfUsers = new HashMap <String, WhatsappUser>();
		_groupName = name;
	}
	
	
	/**
	 * @return the name of the group
	 */
	public String getGroupName() {
		return _groupName;
	}
	
	
	
	/**
	 * This method adds a user to the group
	 * @param userToAdd
	 */
	public void addUserToGroup(WhatsappUser userToAdd){
		synchronized (_containerOfUsers) {
			_containerOfUsers.put(userToAdd.getUserName(), userToAdd);
		}
	}
	
	
	
	/**
	 * This method checks if the User is in the group
	 * @param user
	 * @return true if the user exist else false
	 */
	public boolean existUser(WhatsappUser user){
		return _containerOfUsers.containsKey(user.getUserName());
	}
	
	
	
	/**
	 * This method removes the User from the group
	 * @param userToRemove
	 */
	public void removeUserFromGroup(WhatsappUser userToRemove) {
		synchronized (_containerOfUsers) {
			_containerOfUsers.remove(userToRemove.getUserName());
		}
	}
	
	
	
	
	/**
	 * This method sends the given message to all the users in the group
	 * @param message
	 */
	public void sendMessageToGroup(String message){
		synchronized(_containerOfUsers){	
			for (WhatsappUser user : _containerOfUsers.values()){
				user.sendMessage(message);
			}			
		}
	}

	
	
	/**
	 * This method prints the group
	 * @return string with the group name and all her users
	 */
	public String printGroup() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(_groupName).append("]:");
		
		synchronized(_containerOfUsers){			
			for (WhatsappUser user : _containerOfUsers.values()){
				sb.append("[").append(user.getUserPhoneNumber()).append("],");
			}			
		}
		sb.deleteCharAt(sb.length()-1);
		
		return sb.toString();
	}
	
	
	
}
