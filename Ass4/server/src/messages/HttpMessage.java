package messages;

public class HttpMessage implements Message<String> {

	private String _msg;
	
	public HttpMessage(String msg){
	_msg = msg;	
	}
	
	
	public String getMessage() {
		return _msg;
	}
	
	public String toString(){
		return _msg;
	}

	
}
