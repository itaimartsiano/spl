package messages;

public interface Message<T> {

	/**
	 * This method return the message
	 * @return the message in the type T
	 */
	T getMessage();
}
