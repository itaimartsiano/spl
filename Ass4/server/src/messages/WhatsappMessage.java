package messages;

public class WhatsappMessage implements Message<String> {
	
	private String _requestUri;
	private String _authenticationKey;
	private String _messageToProcess; //the original message but without the /n/n and all the headers
	private String _messageToClient;	//the message that sent back to client include all the headers and /n/n
	
	public WhatsappMessage(String uri , String authenticationKey , String message , String returnMessageToClient){
		_requestUri = uri;
		_authenticationKey = authenticationKey;
		_messageToProcess = message;
		_messageToClient = returnMessageToClient;
	}
	
	@Override
	public String getMessage() {
		return _messageToClient;
	}
	
	public String getMessageToProcess() {
		return _messageToProcess;
	}

	public String get_requestUri() {
		return _requestUri;
	}

	public void set_messageToProcess(String _messageToProcess) {
		this._messageToProcess = _messageToProcess;
	}

	public void set_messageToClient(String _messageToClient) {
		this._messageToClient = _messageToClient;
	}

	public void set_requestUri(String _requestUri) {
		this._requestUri = _requestUri;
	}

	public String get_authenticationKey() {
		return _authenticationKey;
	}

	public void set_authenticationKey(String _authenticationKey) {
		this._authenticationKey = _authenticationKey;
	}
	
	
	
	
	
}
