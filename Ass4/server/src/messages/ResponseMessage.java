package messages;

public class ResponseMessage extends HttpMessage  {   

	private final String HTTP_VERSION = "HTTP/1.1";
	private String _status;
	private String _header;
	private String _messageToClient;	//the message that sent back to client include all the headers and /n/n
	
	public ResponseMessage(String msg , String status ,String header, String msgToClient) {
		
		super(msg);
		_status = status;
		_messageToClient = msgToClient;
		_header = header;
		
	}
	
	/**
	 * This method return the message 
	 */
	public String getMessage() {
		return (HTTP_VERSION +" "+ _status +  _header + "\n\n" + _messageToClient + "\n$");
	}
	
	
	
	public String toString(){
		return (HTTP_VERSION +" "+ _status +  _header + "\n\n" + _messageToClient  + "\n$");
	}
	
	
}
