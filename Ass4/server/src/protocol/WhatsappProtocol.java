package protocol;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import Server.Group;
import Server.UsersDS;
import Server.WhatsappUser;


import messages.WhatsappMessage;

public class WhatsappProtocol implements ServerProtocol<WhatsappMessage>{
	
	
	private UsersDS _usersDS;
	private WhatsappUser _currentUser;		
	
	
	/**
	 * constructor of Whatsapp protocol
	 * @param usersDS - the main data structure where all the data hosted
	 */
	public WhatsappProtocol(UsersDS usersDS){
		_usersDS = usersDS;
		_currentUser = null;
	}
	
	
	/**
	 * this function process the message from http protocol, do what she asked and bring back a response to user
	 * assumes that user has a permission to his request URI - the http protocl check it
	 * assumes that request type is valid and correspond to request URI
	 * @param message - the message to handle
	 * @return - the updated whatsapp message
	 */
	public WhatsappMessage processMessage(WhatsappMessage message) {
		
		if (("login").equals(message.get_requestUri())) loginUser(message);
		else
			if (("logout").equals(message.get_requestUri())) logoutUser(message);
			else
				if (("list").equals(message.get_requestUri())) listRequest(message);
				else
					if (("create_group").equals(message.get_requestUri())) createGroup(message);
					else
						if (("send").equals(message.get_requestUri())) sendMessage(message);
						else
							if (("add_user").equals(message.get_requestUri())) addUserToGroup(message);
							else
								if (("remove_user").equals(message.get_requestUri())) removeUserFromGroup(message);
								else
									if (("queue").equals(message.get_requestUri())) getQueueOfMessages(message);
									else
										if (("exit").equals(message.get_requestUri()))	logoutUser(message);
				
		return message;
	}

	
	
	
	/**
	 * this method return the user all the messages for him
	 * @param message - the message to handle
	 */
	private void getQueueOfMessages(WhatsappMessage message) {
		
		message.set_messageToClient(_currentUser.getMessages());
		
	}

	
	
	/**
	 * this method remove user from specified group
	 * @param message - the message to handle
	 */
	private void removeUserFromGroup(WhatsappMessage message) {
		
		String messageToProcess = message.getMessageToProcess();
		
		if (messageToProcess != null && messageToProcess.matches("Target=(.*)&User=(.*)")){	//correct syntax message
			String [] body = messageToProcess.split("&");
			String [] groupName = body[0].split("=");
			String [] phoneNumber = body[1].split("=");
			StringBuilder sb = new StringBuilder();
			
			Group currentGroup = null;
			
			try {currentGroup = _usersDS.getGroupByName(URLDecoder.decode(groupName[1], "UTF-8"));} 
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			WhatsappUser userToRemove = null;
			try {userToRemove = _usersDS.getUserByPhoneNumber(URLDecoder.decode(phoneNumber[1], "UTF-8"));
			} catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			WhatsappUser sender = _usersDS.getUserByAuthKey(message.get_authenticationKey());
			
			if (currentGroup == null){
				message.set_messageToClient(Errors.ERROR_769);
				return;
			}

			if (!currentGroup.existUser(sender)){			//if the user is not part of this group
				message.set_messageToClient(Errors.ERROR_668);
				return;
			}
			
			if (userToRemove != null && !currentGroup.existUser(userToRemove)){
				message.set_messageToClient(Errors.ERROR_772);
				return;
			}
			
			if (userToRemove != null){
				currentGroup.removeUserFromGroup(userToRemove);
				sb.append("[").append(userToRemove.getUserPhoneNumber()).append("] ");
				sb.append("removed from [").append(currentGroup.getGroupName()).append("]");
				message.set_messageToClient(sb.toString());
			}else{
				message.set_messageToClient(Errors.ERROR_336);
			}
			
		}else{
			message.set_messageToClient(Errors.ERROR_336);
		}
		
	}


	
	/**
	 * this method add user to specified group
	 * @param message - the message to handle
	 */
	private void addUserToGroup(WhatsappMessage message) {
		
		String messageToProcess = message.getMessageToProcess();
		
		if (messageToProcess != null && messageToProcess.matches("Target=(.*)&User=(.*)")){	//correct syntax message
			
			String [] body = messageToProcess.split("&");
			String [] groupName = body[0].split("=");
			String [] phoneNumber = body[1].split("=");
			StringBuilder sb = new StringBuilder();
			
			Group currentGroup = null;
			
			try {currentGroup = _usersDS.getGroupByName(URLDecoder.decode(groupName[1],"UTF-8"));} 
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			WhatsappUser userToAdd = null;
			
			try {userToAdd = _usersDS.getUserByPhoneNumber(URLDecoder.decode(phoneNumber[1],"UTF-8"));} 
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			WhatsappUser sender = _usersDS.getUserByAuthKey(message.get_authenticationKey());
			
			if (currentGroup == null){
				message.set_messageToClient(Errors.ERROR_770);
				return;
			}

			if (!currentGroup.existUser(sender)){			//if the user is not part of this group
				message.set_messageToClient(Errors.ERROR_669);
				return;
			}
			
			if (userToAdd != null && currentGroup.existUser(userToAdd)){
				message.set_messageToClient(Errors.ERROR_142);
				return;
			}
			
			if (userToAdd != null){
				currentGroup.addUserToGroup(userToAdd);
				sb.append("[").append(userToAdd.getUserPhoneNumber()).append("] ");
				sb.append("added to [").append(currentGroup.getGroupName()).append("]");
				message.set_messageToClient(sb.toString());
			}else{
				message.set_messageToClient(Errors.ERROR_242);
			}
			
		}else{
			message.set_messageToClient(Errors.ERROR_242);
		}
		
	}


	
	/** 
	 * this method handle the send message URI request
	 * @param message - the message to handle
	 */
	private void sendMessage(WhatsappMessage message) {
		
		String messageToProcess = message.getMessageToProcess();
		
		if (messageToProcess != null && messageToProcess.matches("Type=Group&Target=(.*)&Content=(.*)")){	//send message to Group
			
			String [] body = messageToProcess.split("&");
			String [] groupName = body[1].split("=");
			String [] msgContent = body[2].split("=");
			StringBuilder sb = new StringBuilder();
			
			Group currentGroup = null;
			
			try {
				currentGroup = _usersDS.getGroupByName(URLDecoder.decode(groupName[1],"UTF-8")); 
				msgContent[1] = URLDecoder.decode(msgContent[1],"UTF-8");
			}
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			WhatsappUser sender = _usersDS.getUserByAuthKey(message.get_authenticationKey());
		
			if (currentGroup != null && !currentGroup.existUser(sender)){			//if the user is not part of this group
				message.set_messageToClient(Errors.ERROR_666);
				return;
			}
			
			if (currentGroup != null){
				
				sb.append("From:[").append(sender.getUserPhoneNumber()).append("/").append(groupName[1]).append("]");
				sb.append("\nMsg:[").append(msgContent[1]).append("]\n");
				
				currentGroup.sendMessageToGroup(sb.toString());
		
			}else	message.set_messageToClient(Errors.ERROR_771);
			
		}else
			
			if (messageToProcess != null && messageToProcess.matches("Type=Direct&Target=(.*)&Content=(.*)")){	//send Direct message 
				
				String [] body = messageToProcess.split("&");
				String [] recievedPhone = body[1].split("=");
				String [] msgContent = body[2].split("=");
				StringBuilder sb = new StringBuilder();
				
				WhatsappUser recievedUser = null;
				try {
					recievedUser = _usersDS.getUserByPhoneNumber(URLDecoder.decode(recievedPhone[1],"UTF-8"));
					msgContent[1] = URLDecoder.decode(msgContent[1],"UTF-8");
					} 
				catch (UnsupportedEncodingException e) {e.printStackTrace();}
				
				WhatsappUser sender = _usersDS.getUserByAuthKey(message.get_authenticationKey());
				
				if (recievedUser != null){
					sb.append("From:[").append(sender.getUserPhoneNumber()).append("]");
					sb.append("\nMsg:[").append(msgContent[1]).append("]\n");
					recievedUser.sendMessage(sb.toString());	
				}else{
					message.set_messageToClient(Errors.ERROR_771);
				}	
			}else																	//there is an error in syntax of message
				{
					if (messageToProcess.matches("Type=(.*)&Target=(.*)&Content=(.*)")){	//missing type
						message.set_messageToClient(Errors.ERROR_836);						
						return;
					}
					message.set_messageToClient(Errors.ERROR_711);							//missing parameters to send message
				}
	}


	
	/**
	 * this method creating new group, if there is no users in the message, the method return error
	 * @param message - the message to handle
	 */
	private void createGroup(WhatsappMessage message) {
		
		String messageToProcess = message.getMessageToProcess();
		
		if (messageToProcess != null && messageToProcess.matches("GroupName=(.*)&Users=(.*)")){		//asked if the syntax is ok
			String [] body = messageToProcess.split("&");
			String [] groupName = body[0].split("=");
			String [] Users = body[1].split("=");
			
			try {Users[1] = URLDecoder.decode(Users[1],"UTF-8");} 
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			String [] usersNameList = Users[1].split(",");
			StringBuilder sb = new StringBuilder();
			
			Group currentGroup = null;
			
			try {currentGroup = _usersDS.addGroup(URLDecoder.decode(groupName[1],"UTF-8"));} 
			catch (UnsupportedEncodingException e) {e.printStackTrace();}
			
			if (currentGroup == null){
				message.set_messageToClient(Errors.ERROR_511);
				return;
			}
			
			for (int i = 0; i < usersNameList.length ; i++){
			
				WhatsappUser userToAdd = _usersDS.getUserByName(usersNameList[i]);
				if (userToAdd == null){
					sb.append(Errors.ERROR_929).append(" [").append(usersNameList[i]).append("]");
					message.set_messageToClient(sb.toString());
					_usersDS.removeGroup(currentGroup);
					return;
				}else{
					_usersDS.addUserToGroup(currentGroup,userToAdd);
				}
				
			}
			
			sb.append("Group [").append(currentGroup.getGroupName()).append("] Created");
			message.set_messageToClient(sb.toString()); 
			
		}
		
		else{
			message.set_messageToClient(Errors.ERROR_675);
		}
		
	}


	
	/**
	 * this method handle the list URI request
	 * @param message - the message to handle
	 */
	private void listRequest(WhatsappMessage message) {
		
		String messageToProcess = message.getMessageToProcess();
		
		if (messageToProcess != null && messageToProcess.matches("List=Users")){		//asked for Users list
			message.set_messageToClient(_usersDS.printUsersList());
		}else
			if (messageToProcess != null && messageToProcess.matches("List=Groups")){		//asked for Groups list
				message.set_messageToClient(_usersDS.printGroups());
			}else
				if (messageToProcess != null && messageToProcess.matches("List=Group&Group=(.*)")){		//asked for some group list
					String [] body = messageToProcess.split("&");
					String [] groupName = body[1].split("=");
					message.set_messageToClient(_usersDS.printGroup(groupName[1]));
				}else
				{
				message.set_messageToClient(Errors.ERROR_273);
				}
		
	}


	
	/**
	 * this method remove user from authentication container  
	 * @param message - the details of message
	 */
	private void logoutUser(WhatsappMessage message) {
		
		message.set_messageToClient("Goodbye");
		_usersDS.removeUserFromAuothenticationContainer(_currentUser.getAuthenticationKey());
	
	}


	
	/**
	 * this method handle the login message' if the user is already exist in UsersDS, the method will add him again to authentication container, else add him as new user
	 * @param message - the message to handle
	 */
	private void loginUser(WhatsappMessage message) {
		

		String messageToProcess = message.getMessageToProcess();
		StringBuilder sb = new StringBuilder();
	
		if (messageToProcess != null && messageToProcess.matches("UserName=([a-zA-Z]*)&Phone=([0-9]*)(.*)") ) 
		{
			String [] body = messageToProcess.split("&");
			String [] userName = body[0].split("=");
			String [] phoneNumber = body[1].split("=");
			
			try {
				_currentUser = _usersDS.getUserByPhoneNumber(URLDecoder.decode(phoneNumber[1],"UTF-8"));
				userName[1] = URLDecoder.decode(userName[1],"UTF-8");
				phoneNumber[1] = URLDecoder.decode(phoneNumber[1],"UTF-8");
			} catch (UnsupportedEncodingException e) {e.printStackTrace();}
			 

				if (_currentUser != null){											//if the user is already exist in the container - did logout in past
					_usersDS.addUserToAuothenticationContainer(_currentUser);
					sb.append("Welcome [").append(_currentUser.getUserName());
					sb.append("]@[").append(_currentUser.getUserPhoneNumber()).append("]");
					message.set_messageToClient(sb.toString());
				}
				else{	
					_currentUser = new WhatsappUser(userName[1],phoneNumber[1],message.get_authenticationKey());
					_usersDS.addUser(_currentUser);
					sb.append("Welcome [").append(_currentUser.getUserName());
					sb.append("]@[").append(_currentUser.getUserPhoneNumber()).append("]");
					message.set_messageToClient(sb.toString());	
				}
				
		}
		else{
			message.set_messageToClient(Errors.ERROR_765);
		}
		
	}


		
	public boolean isEnd(WhatsappMessage msg) {
		return false;
	}
	

}
