package protocol;

public class Errors {

	public static final String ERROR_765 = "ERROR 765: Cannot login, missing parameters";
	public static final String ERROR_766 = "ERROR 766: you are already logged in, or somebody else is loggen in same parameters";
	public static final String ERROR_273 = "ERROR 273: Missing Parameters";
	public static final String ERROR_675 = "ERROR 675: Cannot create group, missing parameters";
	public static final String ERROR_929 = "ERROR 929: Unknown User";
	public static final String ERROR_511 = "ERROR 511: Group Name Already Taken";
	public static final String ERROR_771 = "ERROR 771: Target Does not Exist";
	public static final String ERROR_836 = "ERROR 836: Invalid Type";
	public static final String ERROR_711 = "ERROR 711: Cannot send, missing parameters";
	public static final String ERROR_669 = "ERROR 669: Permission denied";
	public static final String ERROR_242 = "ERROR 242: Cannot add user, missing parameters";
	public static final String ERROR_142 = "ERROR 142: Cannot add user, user already in group";
	public static final String ERROR_770 = "ERROR 770: Target Does not Exist";
	public static final String ERROR_668 = "ERROR 668: Permission denied";
	public static final String ERROR_336 = "ERROR 336: Cannot remove, missing parameters";
	public static final String ERROR_769 = "ERROR 769: Target does not exist";
	public static final String ERROR_666 = "ERROR 666: Permission denied - Irelavant Group";
	public static final String ERROR_772 = "ERROR 772: Permission denied - the user is not part of this group";
	

}
