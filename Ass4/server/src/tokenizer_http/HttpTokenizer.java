package tokenizer_http;


import java.io.IOException;
import java.io.InputStreamReader;
import messages.HttpMessage;
import tokenizer.Tokenizer;


public class HttpTokenizer implements Tokenizer<HttpMessage> {

	private final char _delimiter = '$';
    private InputStreamReader _inputStreamReader;
    private boolean _closed;
   
    
    public HttpTokenizer() {
        _closed = false;
    }
    
    
    
    /**
     * Get the next complete message if it exists, advancing the tokenizer to the next message.
     * @return the next complete message, and null if no complete message exist.
     * @throws IOException 
     */
	public HttpMessage nextMessage() throws IOException {
		if (!isAlive())	throw new IOException("tokenizer is closed");
	 
        String message = null;
        try {
            int c;
            StringBuilder sb = new StringBuilder();
            // read char by char, until encountering the framing character, or
            // the connection is closed.
            while ((c = _inputStreamReader.read()) != -1) {
                if (c == _delimiter)
                    break;
                else
                    sb.append((char) c);
            }
            message = sb.toString();
        } catch (IOException e) {
            _closed = true;
            throw new IOException("Connection is dead");
        }
        HttpMessage ans = new HttpMessage(message);
        return ans;
	}
  
	
	
	/**
	 * @return whether the input stream is still alive.
	 */
	public boolean isAlive() {
		return (!_closed);
	}

	
	
	/**
	 * adding a bufferedReader from which the tokenizer reads the input.
	 */
	public void addInputStream(InputStreamReader inputStreamReader) {
		_inputStreamReader = inputStreamReader;
		
	}
	
	
}
