package tokenizer_http;

import messages.HttpMessage;
import tokenizer.Tokenizer;
import tokenizer.TokenizerFactory;

public class HttpTokenizerFactory implements TokenizerFactory<HttpMessage> {

	public Tokenizer<HttpMessage> create() {
		return new HttpTokenizer();
	}

}
