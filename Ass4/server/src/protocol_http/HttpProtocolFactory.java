package protocol_http;

import messages.HttpMessage;
import protocol.ServerProtocol;
import protocol.ServerProtocolFactory;
import Server.UsersDS;

public class HttpProtocolFactory implements ServerProtocolFactory<HttpMessage> {
	
	private final UsersDS _usersDS; 
	
	/**
	 * constructor of HttpServerProtocolFactory
	 */
	public HttpProtocolFactory(){
		_usersDS = new UsersDS();
	}
	
	/**
	 * create new http protocol - the whatsapp protocol is being construct in the http protocol
	 */
	public ServerProtocol<HttpMessage> create() {
		return new HttpProtocol(_usersDS);
	}

}
