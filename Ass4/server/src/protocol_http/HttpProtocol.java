package protocol_http;

import messages.HttpMessage;
import messages.ResponseMessage;
import messages.WhatsappMessage;
import protocol.ServerProtocol;
import protocol.WhatsappProtocol;
import Server.UsersDS;



public class HttpProtocol implements ServerProtocol<HttpMessage>  {
		
	private UsersDS fUsersDS;
	private WhatsappProtocol fWhatsappProtocol;

	/**
	 * constructor
	 * @param usersDS
	 */
	public HttpProtocol(UsersDS usersDS){
		fUsersDS = usersDS ;
		fWhatsappProtocol = new WhatsappProtocol(usersDS);
	}

	
	
	/**
	 * This method process the message from the client, first check if the structure of the request is valid then ganarate a response
	 * @return  HttpMessage - a response to the client
	 */
	public HttpMessage processMessage(HttpMessage msg) {

		String entireMessage = msg.getMessage();
		
		HttpMessage response = null;

		if ((entireMessage!= null)&&(entireMessage.contains("\n"))){
			String firstLine = (entireMessage.split("\n"))[0];
	
			//checks if the URI exist on the server else return error
			if (!(firstLine.matches("(GET /|POST /)+(logout|queue|login|list|create_group|send|exit|remove_user|add_user)+(.*)"))){
				response = new ResponseMessage(entireMessage,"404","\n" , "Not Found" );
			}
	
			//checks if the structure of the request is valid else return error
			if ( (firstLine.matches("GET /(logout|queue|exit).jsp HTTP/1.1")) ||
				( (firstLine.matches("POST /(login|list|create_group|send|remove_user|add_user).jsp HTTP/1.1")) ) ){
	
				//checks if the user need to log in
				if (firstLine.matches("POST /login.jsp(.*)")) {response = loginMessage(entireMessage);}
				else{ 
					response = otherMessages(entireMessage);
				}
			}
			else { response = new ResponseMessage(entireMessage,"405","\n" , "Invalid Request , Method Not Allowed" );}
		}
		else { response = new ResponseMessage(entireMessage,"405","\n" , "Invalid input" );}

		return response;
	}

	
	
	

	/**
	 * This method checks if the message is an end message
	 */
	public boolean isEnd(HttpMessage msg) {
	
		String entireMessage = msg.getMessage();
		
		if ((entireMessage!= null)&&(entireMessage.matches("(.*)\n(.*)"))){
			String firstLine = (entireMessage.split("\n"))[0];
			
			if (firstLine.matches("(.*)exit(.*)")){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}

	}
	
	/**
	 * handle login messages
	 * @param entireMessage
	 * @return Http message
	 */
	private HttpMessage loginMessage(String entireMessage) {
		
		HttpMessage response = null;
		String authenticationKey;
		String messageToProcess = entireMessage.substring(entireMessage.indexOf("\n")+2, entireMessage.length()-1);
		
		//checks if he doesn't have authentication Key else return error
		if (!(entireMessage.contains("user_auth"))){
			String userName = messageToProcess.substring(messageToProcess.indexOf("=")+1,messageToProcess.indexOf("&"));
			//checks if this is the first time login
			if (fUsersDS.CheckIfUserExist(userName)){
				authenticationKey = fUsersDS.getUserByName(userName).getAuthenticationKey();
			}else {
				authenticationKey = fUsersDS.getAuthenticationKey();
			}
			// process the message in WhatsappProtocol
			WhatsappMessage messageToClient = fWhatsappProtocol.processMessage(new WhatsappMessage("login" , authenticationKey ,messageToProcess ,"")); 
			response = new ResponseMessage(entireMessage,"200","\nSet-Cookie: user_auth="+authenticationKey , messageToClient.getMessage() );
			
		}
		else {
			response = new ResponseMessage(entireMessage,"405","\n" , "Already Logged In , Method Not Allowed");
		}
		
		return response;
	}
	
	
	
	/**
	 * handle all the other messages (not login)
	 * @param entireMessage
	 * @return
	 */
	private  HttpMessage otherMessages(String entireMessage){
		
		HttpMessage response = null;
		String firstLine = (entireMessage.split("\n"))[0];
		String header = entireMessage.split("\n")[1];
		String messageToProcess = entireMessage.substring(entireMessage.indexOf("\n")+2, entireMessage.length()-1);
		String authenticationKey;
		String requestUri;
		
		if (header.contains("user_auth")){  //checks that there is an authentication key in the message
			authenticationKey = header.split("=")[1];
			
			if (firstLine.matches("POST(.*)")){  // only in POST messages the is a message body
				messageToProcess = entireMessage.substring(entireMessage.indexOf("\n\n")+2);	
				if (messageToProcess.length()>3){ messageToProcess = messageToProcess.substring(0,messageToProcess.length()-1);}
			}
			
			//checks if the user have access to the resource else return error
			if (fUsersDS.checkAuthentication(authenticationKey)){
				requestUri = firstLine.substring(firstLine.indexOf("/")+1,firstLine.indexOf("."));
				WhatsappMessage messageToClient = fWhatsappProtocol.processMessage(new WhatsappMessage(requestUri , authenticationKey ,messageToProcess ,""));
				if (requestUri=="exit"){
					response = new ResponseMessage(entireMessage,"200","\n"," exit commend has been recieved, "+messageToClient.getMessage());
				}
				else{ response = new ResponseMessage(entireMessage,"200"," ", messageToClient.getMessage() );}
				
			}else{ response = new ResponseMessage(entireMessage,"403","\n" , "Forbidden" );}
		}else { response = new ResponseMessage(entireMessage,"403","\n" , "Forbidden" );}
		
	return response;
	}
	
	

}