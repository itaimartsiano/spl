package threadPerClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExitCommand implements Runnable {

	@SuppressWarnings("rawtypes")
	private MultipleClientProtocolServer fCurrnetServer; 
	private boolean isClose;

	@SuppressWarnings("rawtypes")
	public ExitCommand(MultipleClientProtocolServer server){
		fCurrnetServer = server;
		isClose = false;
	}


	public void run() {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		String line = null;
		
		try {
			
			while(!isClose){
				line = in.readLine();
				if("Exit".equals(line)){
					isClose = true;
					fCurrnetServer.close();
				}else System.out.println("not recognize the command");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
