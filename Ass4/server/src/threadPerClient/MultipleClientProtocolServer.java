package threadPerClient;


import java.io.IOException;
//import java.io.InputStreamReader;
import java.net.ServerSocket;
//import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import protocol.ServerProtocolFactory;
import protocol_http.HttpProtocolFactory;
import tokenizer.TokenizerFactory;
import tokenizer_http.HttpTokenizerFactory;


public class MultipleClientProtocolServer<T> implements Runnable {
	
	private ServerSocket serverSocket;
	private int listenPort;
	private ServerProtocolFactory<T> _protocolFactory;
	private TokenizerFactory<T> _tokenizerFactory;
	private ExecutorService  _connectionHendlerExecuter;
	private ArrayList<ConnectionHandler<T>> connectionList;

	
	
	public MultipleClientProtocolServer(int port, ServerProtocolFactory<T> protocolFactory, TokenizerFactory<T> tokenizerFactory)
	{
		serverSocket = null;
		listenPort = port;
		_protocolFactory = protocolFactory;
		_tokenizerFactory = tokenizerFactory;
		_connectionHendlerExecuter = Executors.newCachedThreadPool();
		connectionList = new ArrayList<ConnectionHandler<T>>();
	}
	
	

	public void run()
	{
		
		try {
		
			serverSocket = new ServerSocket(listenPort);
			System.out.println("Listening...");
			
		}
		catch (IOException e) {
			System.out.println("Cannot listen on port " + listenPort);
			return;
		}

		while (true)
		{
			try {
				ConnectionHandler<T> newConnection = new ConnectionHandler<T>(serverSocket.accept(), _protocolFactory.create(),_tokenizerFactory.create());
				connectionList.add(newConnection);
				_connectionHendlerExecuter.execute(newConnection);
			}
			catch (IOException e)
			{	
				System.out.println("Failed to accept on port " + listenPort);
				break;
				
			}
		}
	}


	// Closes the connection
	public void close() throws IOException
	{
		for(ConnectionHandler<T> it : connectionList){
			it.close();
		}
		_connectionHendlerExecuter.shutdownNow();
		
		if(serverSocket != null)
			serverSocket.close();
		System.out.println("server is shutting down, bye bye ");
			
	}

	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws IOException
	{
		// Get port
		int port = Integer.decode(args[0]).intValue();

		MultipleClientProtocolServer server = new MultipleClientProtocolServer(port, new HttpProtocolFactory(), new HttpTokenizerFactory());

		Thread serverThread = new Thread(server);
		serverThread.start();
		Thread exitCommand = new Thread(new ExitCommand(server));
		exitCommand.start();
		
		try {
			serverThread.join();
		}
		catch (InterruptedException e)
		{
			System.out.println("Server stopped");
		}



	}
}

