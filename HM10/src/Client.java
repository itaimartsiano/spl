import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;




public class Client {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Scanner in = new Scanner(System.in);
		Socket client = null;
		PrintWriter outputChannel = null;
		
		System.out.println("which host do you want to connect? ");
		String hostServer = "0.0.0.0";
		System.out.println("which port do you want to connect (in the host computer)? ");
		int port = 1200;
		
		System.out.println("Connecting to " + hostServer + ":" + port);
	        
	    // Trying to connect to a socket and initialize an output stream - ---------------------------------------------------------
	    try {
	       client = new Socket(hostServer, port); // host and port
	       outputChannel = new PrintWriter(client.getOutputStream(), true);
	    } catch (UnknownHostException e) {
	    	System.out.println("Unknown host: " + hostServer);
	        System.exit(1);
	    } catch (IOException e) {
	    	System.out.println("Couldn't get I/O to " + hostServer + " connection");
	        System.exit(1);
	    }
	    
	    //connected to Server -----------------------------------------------------------------------------------------------------
	    System.out.println("connected to server in " + hostServer + ":" + port + ", and there is an output channel stream");
	    
        String msg;
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader serverInput = new BufferedReader(new InputStreamReader(client.getInputStream()));
       
        while ((msg = userInput.readLine())!= null)
        {
        	outputChannel.println(msg);
        	if (msg.equals("bye")){
	             break;
	          }
            String answer;
            while (!(answer = serverInput.readLine()).equals("null")){
            	System.out.println(answer);	
            }     
        }
        
        System.out.println("Exiting...");
        
        // Close all I/O
        serverInput.close();
        outputChannel.close();
        userInput.close();
        client.close();
	    
	}

}
