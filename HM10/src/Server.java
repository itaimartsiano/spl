import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		ServerSocket server = null;		//declare of server socket from system
		int port = 1200;						//port
		Runtime runtimeEnv = Runtime.getRuntime();
		
		try{
			server = new ServerSocket(1200);
		}
		catch (IOException e)
		{
			System.out.println("the system cannot listen on port number " + port);
			System.exit(1);
		}
		
		//sever is listening on port and waiting for client ------------------------------------------------------------
		System.out.println(server.getLocalSocketAddress());
		System.out.println("System is listening on port " + port);
		
		Socket client = null;					//declare on client
		try{
			client = server.accept();
		}
		catch (IOException e){
			System.out.println("connecting to client failed");
			System.exit(1);
		}
		
		//connected to client ---------------------------------------------------------------------------------------------
		System.out.println("Accepted connection from client!");	
        System.out.println("The client is from: " + client.getInetAddress() + ":" + client.getPort());

        BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintWriter out = new PrintWriter(client.getOutputStream(), true);
        
        String message;
        
        // server get messages from clients-------------------------------------------------------------------------------
        while((message = in.readLine()) != null){
        	
        	System.out.println("command from client: " + message);
        	       	
        	if (message.equals("bye")){
        		System.out.println("the client ask to disconnect from server");
        		break;
        	}
        	
        	Process responseToCommand = runtimeEnv.exec(message);
        	BufferedReader inputFromCommand = new BufferedReader(new InputStreamReader(responseToCommand.getInputStream()));
        	String printmessage;
        	while ((printmessage = inputFromCommand.readLine()) != null){
        		out.println(printmessage);
        	}
        	out.println(printmessage);
        }
        // client disconnected from server---------------------------------------------------------------------------------
        System.out.println("client disconnected from server");
        out.close();
        server.close();
        client.close();
        in.close();
           
	}
}
