import java.util.concurrent.CountDownLatch;


public class Players implements Runnable {
	
	String name;
	CountDownLatch countDown;
	
	public Players(String name, CountDownLatch latchObject){
		this.name = name;
		countDown = latchObject;
	}
	@Override
	public void run() {
		for (int i= 0; i<50; i++){
			double random = Math.random()*10;
			try {
				Thread.sleep((long) random);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Main.winners.add(name);
		countDown.countDown();
	}

}
