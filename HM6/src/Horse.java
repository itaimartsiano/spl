
       
public final class Horse implements Runnable {
	
	private final String id;
	private int distance = 0;
	private final FinishingLine fline;
	
	public Horse(String id, FinishingLine f){
		fline = f;
		this.id = id;
	}
	
	public synchronized void run() {
		
		while (distance < 100) {
			
			try{
				Thread.sleep((long) (Math.random()*200));
				
			}
			catch (Exception e){}
			
			//there is an option to use the wait function but remember to use synchronize before;
			distance++;
		}
	
		fline.arrive(this);
	}
	
	 public String getId(){
		return id;
	 }
	 
	 
}
