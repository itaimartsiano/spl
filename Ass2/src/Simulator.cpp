#include "../include/mainObjects.h"
#include "../include/SystemEvents.h"
#include "../include/Ini.h"
#include "../include/Commands.h"
#include "../include/Simulator.h"
#include <iostream>
#include <map>


using namespace std;
class Commands;

//constructor
Simulator::Simulator() : reader(), roadDB(), carDB(), junctionDB(), commandsDB(),EventDB(),
			defaultTimeSlice(0), maxTime(0), minTime(0), maxSpeed(0), clock(1)
{
	reader.readConfig(defaultTimeSlice, maxTime, minTime, maxSpeed);
}


//destructor
Simulator::~Simulator(){
	for (map <string,Road*>::iterator it = roadDB.begin(); it!=roadDB.end(); ++it)
	    delete it->second;
	for (map <string,Car*>::iterator it = carDB.begin(); it!=carDB.end(); ++it)
	    delete it->second;
	for (map <string,Junction*>::iterator it = junctionDB.begin(); it!=junctionDB.end(); ++it)
		delete it->second;
	while(!EventDB.empty()){
		delete EventDB.front();
		EventDB.pop_front();
	}
	while(!commandsDB.empty()){
			delete commandsDB.front();
			commandsDB.pop_front();
	}
}


void Simulator::initialize() {
//read the files
	reader.readRoadMap(roadDB, junctionDB, maxSpeed, defaultTimeSlice);
	reader.readCommands(commandsDB, carDB, roadDB, junctionDB);
	reader.readEvents(EventDB, carDB, roadDB, junctionDB);
//sort the commands and the events
	commandsDB.sort(sortByTimeCommand);
	EventDB.sort(sortByTime);
// intialize the time slice of all the roads
	for (map<string, Road*>::iterator currentRoad = roadDB.begin();
			currentRoad != roadDB.end(); currentRoad++) {
		(*currentRoad).second->setTimeSliceToOpen(defaultTimeSlice);
	}
}


bool Simulator::initialize_Succeed(){
	bool ans = false;
	if (!roadDB.empty() && !EventDB.empty() && !junctionDB.empty()){
		ans = true;
	}
	return ans;
}



//This method passes on all the event in the current time unit end execute them
void Simulator::executeEvents() {
	list<Event*>::iterator theEvent = EventDB.begin();

	while ((theEvent) != EventDB.end() && (*theEvent)->getTimeEvent()== clock) { //as long as we didn't finish all the event and the event time equal the clock
		(*theEvent)->performEvent();
		delete EventDB.front(); //deldete the event
		EventDB.pop_front();  //take the event out from the list
		theEvent = EventDB.begin();
	}
}


//This method passes on all the commands in the current time unit end execute them
void Simulator::executeCommands() {
	list<Commands*>::iterator theCommand = commandsDB.begin();

	while (((*theCommand)->getTimeCommand() == clock) && (!commandsDB.empty())) { //as long as we didn't finish all the commends and the commend time equal the clock
		if ((*theCommand)->getType() == "termination"){
			(*theCommand)->performCommand();
			delete commandsDB.front();
			commandsDB.pop_front();
			theCommand = commandsDB.begin();
			while (!commandsDB.empty()){
				if ((*theCommand)->getType() == "termination"){
					cout<<"there is more then one termination command in Commands.ini"<<endl;
				}else	cout<<"there is more Commands after termination was declared"<<endl;
				delete commandsDB.front();
				commandsDB.pop_front();
				theCommand = commandsDB.begin();
			}
		}
		else{
			if (commandsDB.size() == 1){
				cout <<"there was no command of termination"<<endl;
			}
			(*theCommand)->performCommand();
			delete commandsDB.front();
			commandsDB.pop_front();
			theCommand = commandsDB.begin();
		}
	}
}



// This method advance all the cars in each road
void Simulator::advanceCarsInRoads() {

	for (map<string, Car*>::iterator currentCar = carDB.begin(); currentCar != carDB.end(); currentCar++) {    //passes on all the cars
		if ((((*currentCar).second->getRoad()) != NULL) //if the car is in a road and not in the end
				&& (((*currentCar).second->getRoad()->getTheLastTimeOfUpdate()) < (clock))) {  //check if we  haven't advanced the cars in this road by the time the road has been updated
			((*currentCar).second->getRoad())->advanceCars();					//advance all the cars in this road
			((*currentCar).second->getRoad())->setTheLastTimeOfUpdate(clock); //update the time
		}
	}
}



//This method advance the cars in each junction
void Simulator::advanceCarsInJunctions() {

	bool searching;

	for (map<string, Junction*>::iterator currentJunc = junctionDB.begin();
			currentJunc != junctionDB.end(); currentJunc++) {    //passes on all the junctions
		Road* currentRoad =(
				(((*currentJunc).second)->getIncomingRoads())[((*currentJunc).second->getLightIndex())
						% ((*currentJunc).second->getIncomingRoads().size())]);   //go to the road with the green light

		//initializing values for searching car to pass junction
		searching = true;
		list<Car*>::iterator it = currentRoad->getCarsInRoad().begin();

		//searching suitable car to pass junction (at the end of the road and not faulty)
		while ( searching && (it != currentRoad->getCarsInRoad().end())  && ((*it)->getCarLocation()== currentRoad->getLength()))
		{
			// checks if there is a car in the road and she is at the end of the road (waiting to pass) and not fault
			if ((*it)->getFaultyTime() == 0)
			{
				(*it)->changeRoad();  // move the car from this road to the next
				currentRoad->setCarsPassesCounter(currentRoad->getCarsPassesCounter()+1); //update the counter of the cars the passes
				currentRoad->getCarsInRoad().erase(it); //remove the car from this road
				searching = false;
			}	
			else
				it++;
		}

		currentRoad->setGreenLightCounter(currentRoad->getGreenLightCounter()-1); //update the timer for the green light

		if (currentRoad->getGreenLightCounter() == 0) { //if the time slice for this road ended
			currentRoad->setGreenLightCounter(-1);  //give this road red light

			if ((*currentRoad).getTimeSliceToOpenLight() == (*currentRoad).getCarsPassesCounter()) {  //this road fully used it's time slice
				(*currentRoad).setTimeSliceToOpen( min((*currentRoad).getTimeSliceToOpenLight() + 1, maxTime));
			} 
			else 
			{
				if ((*currentRoad).getCarsPassesCounter() == 0) //this road haven't used it's time slice at all
					(*currentRoad).setTimeSliceToOpen( max((*currentRoad).getTimeSliceToOpenLight() - 1, minTime));
			}

			(*currentJunc).second->setLightIndex((*currentJunc).second->getLightIndex()+1); //change the index to the index of the road with green light
			currentRoad =(
					((*currentJunc).second->getIncomingRoads())[((*currentJunc).second->getLightIndex())
							% ((*currentJunc).second->getIncomingRoads().size())]);  //go to the road with the green light
			currentRoad->setGreenLightCounter( currentRoad->getTimeSliceToOpenLight());  // set the counter to the time slice
			currentRoad->setCarsPassesCounter(0);  //reset the counter of the cars that passes
		}
	}
}



// This method manage the simulation
void Simulator::Run() {
	this->initialize();  //reads all the files
	list<Commands*>::iterator Command = commandsDB.begin();
	if (initialize_Succeed()){
		while (!commandsDB.empty()) {			//runs until there are no commends
			executeEvents();					// perform all the events in this time unit
			updateCarHistory();					// update the history in all the cars
			executeCommands();					// perform all the commands in this time unit
			advanceCarsInRoads();				// advance the cars in the roads
			advanceCarsInJunctions();			// advance the cars in the junctions
			clock++;							//advance the clock
			Command = commandsDB.begin();
		}
	}else cout<<"there was a problem with supplied files - the program stop before start simulate"<<endl;
}



//This method update the history of the cars
void Simulator::updateCarHistory() {
	for (map<string, Car*>::iterator currentCar = carDB.begin();
			currentCar != carDB.end(); currentCar++) {   //passes on all the cars
		(*currentCar).second->updateMyHistory(clock);   //update their history
	}

}
