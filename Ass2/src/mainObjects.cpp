#include "../include/mainObjects.h"
#include <iostream>
#include <map>
#include <list>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace std;
using namespace boost;
using namespace property_tree;

//constructor
Road::Road(string startJunc, string endJunc, int lengthRoad, int & max_Speed) :
	start_junction (startJunc), length_(lengthRoad), MAX_SPEED(max_Speed), id_(startJunc + ',' + endJunc),lastTimeUpdate_(0),speedRoad_(0),timeSliceOpen_(0),greenLightCounter_(-1),
	carsPassesCounter_(0), carInRoad_(){
}


//destructor
Road::~Road(){
}


//getters
string Road::getStartJunction(){
	return start_junction;
}

int Road::getLength(){
	return length_;
}

string Road::getId(){
	return id_;
}

int Road::getTheLastTimeOfUpdate(){
	return lastTimeUpdate_;
}

int Road::getRoadSpeed(){
	return speedRoad_;
}

int Road::getTimeSliceToOpenLight(){
	return timeSliceOpen_;
}

int Road::getGreenLightCounter(){
	return greenLightCounter_;
}

int Road::getCarsPassesCounter(){
	return carsPassesCounter_;
}

list<Car*> & Road::getCarsInRoad(){
	return carInRoad_;
}


//setters
void Road::setGreenLightCounter(int greenLightTime ){
	greenLightCounter_=greenLightTime;
}

void Road::setTimeSliceToOpen(int timeSlice){
	timeSliceOpen_=timeSlice;
}

void Road::setTheLastTimeOfUpdate(int updateTime){
	lastTimeUpdate_ = updateTime;
}

void Road::setCarsPassesCounter(int counter){
	carsPassesCounter_=counter;
}

void Road::setLength(int roadLength){
	length_=roadLength;
}


void Road::printRoad() {
	cout << "-------print road----------" << endl;
	cout << "id is : " << id_ << endl;
	cout << "length is : " << length_ << endl;
	cout << "list of cars in road: " << endl;
	for (list<Car*>::iterator it = carInRoad_.begin(); it != carInRoad_.end();
		++it)
	cout << (*it)->getCarId() << "," << endl;
}



//this method passes all the cars that their location is equal to the length of the road and adds them into one string and return it
string Road::queueForJunction() {
	string line = "";
	list<Car*>::iterator theCar = carInRoad_.begin();
	bool noMoreCar = false;

	while (!noMoreCar && theCar != carInRoad_.end()) {
		if ((*theCar)->getCarLocation() == length_) {
			line = "(" + (*theCar)->getCarId() + ")" + line;
		} else
			(noMoreCar = true);
		++theCar;
	}
	return line;
}


//this method return the time slice of the road and the counter that counts how much time the road had green light
string Road::timeSliceForJunction() {
	string timeSlice = (string) (lexical_cast<string>(timeSliceOpen_));
	string lightCounter = (string) (lexical_cast<string>(greenLightCounter_));
	string ans = "(" + timeSlice + "," + lightCounter + ")";
	return ans;
}


//this method advances all the cars on this road
void Road::advanceCars() {
	speedRoad_ = static_cast<int>(ceil(
			(static_cast<double> (length_) / static_cast<double> (carInRoad_.size()) )));   //first calculate the speed of the road
	int tmpLocation = 0;
	int countFaultyCars = 0;

	// ---------------------passes on all the cars on the road-------------------
	for (list<Car*>::iterator PointerCar = carInRoad_.begin();
		PointerCar != carInRoad_.end(); ++PointerCar) {

		if ((*PointerCar)->getFaultyTime()> 0) { //if the car is fault we add to our fault car counter and decrease her fault time
			(*PointerCar)->updateFaultyTime(-1);
			countFaultyCars++;
			list<Car*>::iterator tmpPointer = PointerCar;
			PointerCar++;
			while ((PointerCar != carInRoad_.end())	&& ((*PointerCar)->getCarLocation()== (*tmpPointer)->getCarLocation())) {
			// checking if there are faulty car in the same location as the first one and add to our counter
				if ((*PointerCar)->getFaultyTime()> 0) {
					(*PointerCar)->updateFaultyTime(-1);
					countFaultyCars++;
				} else { //the car is not fault but in the same location so the fault car doesn't effect it
						if (speedRoad_ > MAX_SPEED) {
							tmpLocation = (*PointerCar)->getCarLocation()+ MAX_SPEED; //check if the car will pass the length of the road when we add the speed
							if (tmpLocation > length_) {
								(*PointerCar)->setLocation( length_);
							} else {
								(*PointerCar)->setLocation(tmpLocation);
							}
						} else { //the speed of the road is ok <=MAX_SPEED
							tmpLocation = (*PointerCar)->getCarLocation() + speedRoad_; //check if the car will pass the length of the road when we add the speed
							if (tmpLocation > length_) {
								(*PointerCar)->setLocation(length_) ;
							} else {
								(*PointerCar)->setLocation(tmpLocation);
							}
						}
				}
				PointerCar++;
			}
			PointerCar--; //the cars are not in the same location anymore
			speedRoad_ =
					static_cast<int>(ceil(
							(static_cast<double> (speedRoad_)
									/ static_cast<double> (pow(2.0, countFaultyCars))) )); //recalculate the speed base on the number of fault cars
			countFaultyCars = 0; //restart the counter so we won't double count fault cars

		}else { // the car is not fault so we add to her location the speed

				if (speedRoad_ > MAX_SPEED) {
					tmpLocation = (*PointerCar)->getCarLocation()+ MAX_SPEED; //check if the car will pass the length of the road when we add the speed
					if (tmpLocation > length_) {
						(*PointerCar)->setLocation(length_);
					} else {
						(*PointerCar)->setLocation(tmpLocation);
					}
				} else {
					tmpLocation = (*PointerCar)->getCarLocation()+ speedRoad_; //check if the car will pass the length of the road when we add the speed
					if (tmpLocation > length_) {
						(*PointerCar)->setLocation(length_);
					} else {
						(*PointerCar)->setLocation(tmpLocation);
					}
				}
		}
	}
	carInRoad_.sort(sortByLocation); //sorts all the cars in the road base on their location
}


//constructor
Car::Car(string id, string strRoadPlan, map<string, Road*> & roadBase) :
	id_(id), roadPlan(), faultyTime_(0), speed_(0), location_(0),  history_(""),road(0){

	roadPlan = RoadPlanConverter(strRoadPlan, roadBase);

}

//This method helps the operator = function
Car & Car::copy(const Car& car){
 if (this == &car) {
		return *this;
	  }
 id_=car.id_;
 roadPlan=car.roadPlan;
 faultyTime_=car.faultyTime_;
 speed_=car.speed_;
 location_=car.location_;
 history_=car.history_;
 road=car.road;
 return *this;
}


//copy constructor
Car::Car (const Car& newCar)
:id_(newCar.id_),roadPlan(newCar.roadPlan),faultyTime_(newCar.faultyTime_),speed_(newCar.speed_),location_(newCar.location_),history_(newCar.history_),road(newCar.road){

}


Car & Car::operator=(const Car& theCar){
	return this->copy(theCar);
}


//this method convert the string road plan that we were given to a list with pointers to roads
list<Road*> Car::RoadPlanConverter(string strRoadPlan,map<string, Road*>& roadBase) {
	unsigned int beg = 0, mid = 0;
	int sof;
	list<Road*> ans;

	if (strRoadPlan.size()!=0){
		while (mid < strRoadPlan.length()) {

			mid = strRoadPlan.find(',', (beg));
			sof = strRoadPlan.find(',', (mid + 1));
			if (sof == -1)
					sof = strRoadPlan.length();
			string tmp = strRoadPlan.substr(beg, (sof - beg));
			if (roadBase.find(tmp) != roadBase.end()){
					Road* roadname = roadBase[tmp];
					ans.push_back(roadname);
			}else{
					cout<<"there is no road called: "<<tmp<<endl;
			}
			beg = mid + 1;
			mid = sof;

		}
	}else cout<<"the road plan is empty"<<endl;
	return ans;
}


//getters
string Car::getCarId(){
	return id_;
}

list<Road*> & Car::getRoadPlan(){
	return roadPlan;
}

int Car::getFaultyTime(){
	return faultyTime_;
}

int Car::getCarSpeed(){
	return speed_;
}

int Car::getCarLocation() const{
	return location_;
}

string Car::getCarHistory(){
	return history_;
}

Road* Car::getRoad(){
	return road;
}


//setters
void Car::updateFaultyTime(int faultTime){
	faultyTime_ += faultTime;
}

void Car::setCarSpeed(int carSpeed){
	speed_=carSpeed;
}

void Car::setLocation(int location){
	location_=location;
}

void Car::setRoad(Road* newRoad){
	road=newRoad;
}


void Car::printCar() {
	cout << "-------print car----------" << endl;
	cout << "the id is:" << id_ << '\n'; //checking if works
	cout << "on road:" << road->getId()<< '\n';
	cout << "the last location is:" << location_ << '\n';
	cout << "time to stop faulty:" << faultyTime_ << '\n';
	for (std::list<Road*>::iterator it = roadPlan.begin(); it != roadPlan.end();
			++it)
	cout << ((*it)->getId()) << ", " << endl;

}


//destructor
Car::~Car() {
}


//this method return the name and location of the car for a report
string Car::getTheNameAndLocation() {
	string Mylocation = (string) (lexical_cast<string>(location_));
	string ans = "(" + id_ + "," + Mylocation + ")";
	return ans;
}


//This method update the history of the car
void Car::updateMyHistory(int time) {
	if (road != NULL) {
		history_ = history_ + "(" + (string) (lexical_cast<string>(time)) + ","
		+ (road->getId()) + "," + (string) (lexical_cast<string>(location_))
		+ ")";
	}
}


// This method moves the car to it's next road
void Car::changeRoad() {
	location_ = 0;
	roadPlan.pop_front(); //discard the last road

	if (!roadPlan.empty()) { //checks that there is at least one road to move to it
		road = roadPlan.front(); //update the pointer of the road to the current road (in the car)
		road->getCarsInRoad().push_back(this); //insert the car into the list of cars in the current road
	} else //the car passes all the roads in her road plan
		road = NULL;

}

//constructor
Junction::Junction(string name) :id_(name), lightIndex_(0), incomingRoads_()
{
}


//destructor
Junction::~Junction(){

}



//getters
string Junction::getJuncId(){
	return id_;
}

int Junction::getLightIndex(){
	return lightIndex_;
}

vector<Road*> & Junction::getIncomingRoads(){
	return incomingRoads_;
}



//setters
void Junction::setId(string newId){
	id_=newId;
}
void Junction::setLightIndex(int newIndex){
	lightIndex_=newIndex;
}


//This method insert a road to it's vector of incoming roads
void Junction::insertRoad(string roadId, map<string, Road*>& roadMap,int defaultTimeSlice){
	if (roadMap.find(roadId) != roadMap.end()){ //checks that the road is in the data base
		Road* roadname = roadMap[roadId];
		if (incomingRoads_.empty()) { //checks if this road is the first incoming road
			roadname->setGreenLightCounter(defaultTimeSlice); //change the road light to green
			incomingRoads_.push_back(roadname);
		} else {
			incomingRoads_.push_back(roadname);
		}
	}else cout << "try to enter wrong roadId into map"<<endl;
}


void Junction::printJuction() {
	cout << "-------print junction----------" << endl;
	cout << "the id is:" << id_ << '\n';
	cout << "the light index is: " << lightIndex_ << '\n';
	cout << "the incoming roads is: " << '\n';
	for (unsigned int i = 0; i < incomingRoads_.size(); i++) {
		cout << ((*incomingRoads_[i]).getId()) << '\n';
	}
	cout << endl;
}


bool sortByLocation(const Car* car1, const Car* car2) {
return ((car1->getCarLocation()) > (car2->getCarLocation()));
}
