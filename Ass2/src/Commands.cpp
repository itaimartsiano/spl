
#include "../include/Commands.h"
#include "../include/mainObjects.h"
//#include "../include/Ini.h"
#include <iostream>
#include <vector>
#include <map>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>

class Car;
using namespace std;
using namespace boost;
using namespace property_tree;

ptree pta;

//constructor
Commands::Commands (string type,int time):type_(type),time_(time){

}

//destructor
Commands::~Commands(){

}

//getters
string Commands::getType(){

	return type_;
}

int Commands::getTimeCommand() const{

	return time_;
}


//constructor
Termination::Termination(string type,int time):Commands(type,time){

}

//destructor
Termination::~Termination(){};

void Termination::printCommand(){
	cout<<"the type is: "<< type_<<endl;
	cout<<"the time for command is: "<<time_<<endl;
}

//This method writes the file Reports.ini
void Termination::performCommand(){

	ini_parser::write_ini("Reports.ini",pta);
}


//constructor
Report::Report(string type,int time,string id):Commands(type,time),id_(id){

}

//destructor
Report::~Report(){

}


//constructor
CarReport::CarReport(string type,int time,string id,string carId,map<string,Car*> & cars_DB):Report(type,time,id),car_id(carId),carsDB(cars_DB){

}

void CarReport::printCommand(){
	cout<<"------print car report----------"<<endl;
	cout<<"the type is: "<< type_<<endl;
	cout<<"the time for command is: "<<time_<<endl;
	cout<<"the report id is: "<<id_<<endl;
	cout<<"the car id is: "<<car_id<<endl;
}

//destructor
CarReport::~CarReport(){

}

//This method execute the command by calling the function writeReport
void CarReport::performCommand(){
	writeReport();
}


//This method insert to the ptree the car report
void CarReport::writeReport(){
	if (carsDB.find(car_id) != carsDB.end()){
		if (pta.find(id_) == pta.not_found()){
			pta.add(id_+"."+"carId",car_id);
			pta.add(id_+"."+"history",(*carsDB[car_id]).getCarHistory());
			pta.add(id_+"."+"faultyTimeLeft",(*carsDB[car_id]).getFaultyTime());
		}else cout << "there is two car report with the same id" << endl;
	}else cout << "(command car report) there is no car with the name: "<<car_id<<endl;
}


//constructor
RoadReport::RoadReport(string type,int time,string id,string startJunc,string endJunc,map <string,Road*> & road_DB):Report(type,time,id)
,start_junction(startJunc),end_junction(endJunc),roadDB(road_DB)
{
}


//destructor
RoadReport::~RoadReport(){

}


//This method execute the command by calling the function writeReport
void RoadReport::performCommand(){
	writeReport();
}


//This method insert to the ptree the road report
void RoadReport::writeReport(){
	string road_ = start_junction+","+end_junction;
	if (roadDB.find(road_) != roadDB.end()){     //verify that the road is in the data base of the simulation
		pta.add(id_+"."+"startJunction", start_junction);
		pta.add(id_+"."+"endJunction", end_junction);
		string carsInRoad="";
		for (list <Car*>::iterator theCar = (*roadDB[road_]).getCarsInRoad().begin(); theCar!=(*roadDB[road_]).getCarsInRoad().end(); ++theCar){
			carsInRoad=(*theCar)->getTheNameAndLocation() + carsInRoad;
		}
	pta.add(id_+"."+"cars",carsInRoad);
	}else cout<<"there is no road with the name: "<< road_<<endl;
}


void RoadReport::printCommand(){
	cout<<"-------print road report----------"<<endl;
	cout<<"the type is: "<< type_<<endl;
	cout<<"the time for command is: "<<time_<<endl;
	cout<<"the report id is: "<<id_<<endl;
	cout<<"the start junction is: "<<start_junction<<endl;
	cout<<"the end junction is: "<<end_junction<<endl;
}


//constructor
JunctionReport::JunctionReport(string type,int time,string id,string juncId, map <string, Junction*> & junction_DB)
:Report(type,time,id),junction_id(juncId), junctionDB(junction_DB){

}


//destructor
JunctionReport::~JunctionReport(){
}


//This method execute the command by calling the function writeReport
void JunctionReport::performCommand(){
	writeReport();
}


//This method insert to the ptree the junction report
void JunctionReport::writeReport(){
	if (junctionDB.find(junction_id) != junctionDB.end()){   //verify that the junction is in the data base of the simulation
		pta.add(id_+"."+"junctionId",junction_id);
		Junction * theJunction=junctionDB[junction_id];
		string timeSlice="";
		pta.add(id_+"."+"timeSlices",timeSlice);
			for (unsigned int i=0; i<(*theJunction).getIncomingRoads().size();i++ ){  //for each incoming road of the junction we take the data for the report
				Road* theRoad=((*theJunction).getIncomingRoads())[i];
				string slices=(*theRoad).timeSliceForJunction();
				string carQueue=(*theRoad).queueForJunction();
				timeSlice=timeSlice+slices;
				pta.add(id_+"."+(*theRoad).getStartJunction(),carQueue);
			}
		pta.put(id_+"."+"timeSlices",timeSlice);
	}
	else cout <<"there is no junction with the name: "<<junction_id<<endl;
}

void JunctionReport::printCommand(){
	cout<<"-------print junction report----------"<<endl;
	cout<<"the type is: "<< type_<<endl;
	cout<<"the time for command is: "<<time_<<endl;
	cout<<"the report id is: "<<id_<<endl;
	cout<<"the junction id is: "<<junction_id<<endl;
}

bool sortByTimeCommand (const Commands* command1, const Commands* command2)
{
	return ((command1->getTimeCommand())< (command2->getTimeCommand()));
}
