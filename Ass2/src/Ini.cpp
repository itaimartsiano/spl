#include "../include/Ini.h"
#include "../include/mainObjects.h"
#include "../include/SystemEvents.h"
#include "../include/Commands.h"
#include <iostream>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <map>
#include <list>

using namespace std;
using namespace boost;
using namespace property_tree;

//constructor
readerClass::readerClass(){}


//destructor
readerClass::~readerClass() {
}


//This method reads from the file RoadMap.ini and creates the roads and junctions
void readerClass::readRoadMap(map <string,Road*> & roadDB, map <string,Junction*> & junctionDB,int & max_speed, int defaultTimeSlice){
  ptree pt;                         //initialize property tree
  string startJunction;
  string endJunction;
  int length = 0;

  try{
	ini_parser::read_ini("RoadMap.ini", pt);    //reads the file
    for (ptree::const_iterator section = pt.begin(); section != pt.end(); section++) {
		endJunction = (string) section->first;
		Junction * insjunc = new Junction(endJunction);   //creates the junction

		for (ptree::const_iterator property = section->second.begin(); property != section->second.end(); property++) {
			startJunction = (string) property->first;
			length = (int)(lexical_cast<int>(property->second.data()));
			(roadDB)[startJunction+","+endJunction] = (new Road(startJunction,endJunction, length,max_speed));   //creates the road
			insjunc->insertRoad(startJunction+","+endJunction,roadDB, defaultTimeSlice);                //insert the road to a vector of incoming roads of the junction
			(junctionDB)[endJunction] = (insjunc);   //insert the junction to the data base
		}
    }
  }
  catch (const ptree_error &e){
  	  cout<<"When trying to read from file RoadMap.ini there was an error"<<endl;
  }
  catch (const bad_lexical_cast &t){
	  cout<<"When trying read the file RoadMap.ini, the program had a bad lexical cast"<<endl;
  }
}


//This method reads from the file Events.ini and creates the events of the simulation and insert them to the data base
void readerClass::readEvents(list <Event*> & eventList, map <string, Car*> & carDB, map <string, Road*> & roadDB, map <string, Junction*> & junctionDB) {
  string type,carId,roadplan;
  int timeEvent,timeFault;
  bool addcar=false;
  ptree pt;                           //initialize property tree

  try {
  ini_parser::read_ini("Events.ini", pt);       //reads the file
  for (ptree::const_iterator section = pt.begin();section != pt.end(); section++) {
	  for (ptree::const_iterator property =    section->second.begin();property != section->second.end(); property++) {
        if ((property->first) =="type"){
        	type=(property->second.data());
        }else
        if ((property->first) =="time"){
        	timeEvent=(int)(lexical_cast<int>(property->second.data()));

        }else
        if ((property->first) =="carId"){
        	carId=(property->second.data());
        }else
        if (addcar){
        	roadplan=(property->second.data());
        };
        if ((property->second.data()) =="car_arrival"){
                addcar=true;
        }else
        if ((property->first) =="timeOfFault"){
        	timeFault=(int)(lexical_cast<int>(property->second.data()));
        };
	  };

	  //creates and insert the event into the data base
	  if (addcar){
		  eventList.push_back((Event*)new AddCarEvent(roadplan,type,carId,timeEvent,carDB,roadDB));
	  }
	  else  eventList.push_back((Event*)new CarFaultEvent( type,timeEvent,carId,carDB,timeFault));

	  addcar=false;

   }
  }
  catch (const ptree_error &e){
	  cout<<"When trying to read from file Events.ini there was an error"<<endl;
  }
  catch (const bad_lexical_cast &t){
	  cout<<"When trying read the file Events.ini, the program had a bad lexical cast"<<endl;
  }
}



//This method reads from the file Configuration.ini and update the configuretion parameters
void readerClass::readConfig(int & DefaultTime,int & MaxTime, int & MinTime,int & maxSpeed) {
	ptree pt;               //initialize property tree
	try {
		ini_parser::read_ini("Configuration.ini", pt);            //reads the file
		for (ptree::const_iterator section = pt.begin();section != pt.end(); section++) {
			for (ptree::const_iterator property = section->second.begin();property != section->second.end(); property++) {

				if ((property->first) =="MAX_SPEED"){
					maxSpeed=(int)(lexical_cast<int>(property->second.data()));
				};
				if ((property->first) =="DEFAULT_TIME_SLICE"){
					DefaultTime=(int)(lexical_cast<int>(property->second.data()));
				};
				if ((property->first) =="MAX_TIME_SLICE"){
					MaxTime=(int)(lexical_cast<int>(property->second.data()));
				};
				if ((property->first) =="MIN_TIME_SLICE"){
					MinTime=(int)(lexical_cast<int>(property->second.data()));
				};

			}

		}
	}
	catch (const ptree_error &e){
	  cout<<"When trying to read from file configuration.ini there was a problem"<<endl;
	}
	catch (const bad_lexical_cast &t){
	  cout<<"When trying read the file configuration.ini, the program had a bad lexical cast"<<endl;
	}
}



//This method reads from the file Commands.ini and creates the commands of the simulation and insert them into the data base
void readerClass::readCommands(list <Commands*> & commandList, map <string, Car*> & carDB, map <string, Road*> & roadDB, map <string, Junction*> & junctionDB){
	string type,reportId,carId,startJunction,endJunction,junctionId;
	int timeCommand;
	ptree pt;                      //initialize property tree

	try {
		ini_parser::read_ini("Commands.ini", pt);              //reads the file
		for (ptree::const_iterator section = pt.begin();section != pt.end(); section++) {
			  for (ptree::const_iterator property =    section->second.begin();property != section->second.end(); property++) {

				  if ((property->first) =="type"){
							type=(property->second.data());
						 }else
						 if ((property->first) =="time"){
							timeCommand=(int)(lexical_cast<int>(property->second.data()));
						 }else
						 if ((property->first) =="id"){
							reportId=(property->second.data());
						 }else
						 if ((property->first) =="carId"){
							 carId=(property->second.data());
						 }else
						 if ((property->first) =="startJunction"){
							 startJunction=(property->second.data());
						 }else
						 if ((property->first) =="endJunction"){
							 endJunction=(property->second.data());
						 }else
						 if ((property->first) =="junctionId"){
							junctionId=(property->second.data());
						 };

			  }   //creates the commands and insert them into the data base
			  if (type=="termination"){
				  commandList.push_back(new Termination(type,timeCommand));

			  }else
				 if(type=="car_report"){
					commandList.push_back(new CarReport(type,timeCommand,reportId,carId, carDB));
				 }else
					if (type=="road_report"){
						commandList.push_back(new RoadReport(type,timeCommand,reportId,startJunction,endJunction,roadDB));
					}else
						if(type=="junction_report"){
							commandList.push_back(new JunctionReport(type,timeCommand,reportId,junctionId,junctionDB));
						}
		}
	}
	catch (ini_parser_error & e){
		cout<<"When trying read the file Commands.ini, there was a duplicate section name"<<endl;
	}
	catch (const bad_lexical_cast &t){
		cout<<"When trying read the file Commands.ini, the program had a bad lexical cast"<<endl;
	}

}
