
#include "../include/Ini.h"
#include "../include/Simulator.h"
#include "../include/mainObjects.h"
#include "../include/SystemEvents.h"
#include "../include/Commands.h"

#include <iostream>

using namespace std;

int main(){

	Simulator sim=Simulator();    // create the simulator
	sim.Run();                    // run the simulation


    return 1;
}


