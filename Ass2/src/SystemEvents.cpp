#include "../include/SystemEvents.h"
#include "../include/mainObjects.h"
#include <iostream>
#include <map>


using namespace std;

//constructor
Event::Event (string type,string Id, int timeof,map<string,Car*> & car_DB):
		type_(type),carId_(Id), timeEvent_(timeof),carsDB(car_DB){
}

//destructor
Event:: ~Event(){
}

//getters
string Event::getType(){
		return type_;
}

string Event::getCarId(){
		return carId_;
}

int Event::getTimeEvent() const{
		return timeEvent_;
}



//constructor
AddCarEvent::AddCarEvent (string planRoad,string type,string Id,int Mtime,map<string,Car*> & car_DB,map <string,Road*> & road_DB):
		Event(type,Id,Mtime,car_DB),MroadPlan_(planRoad),roadDB(road_DB){
}


// This method add car to the simulation
void AddCarEvent:: performEvent(){
	if (carsDB.find(carId_) == carsDB.end()){                  //checks if there is no other car with the same id in the data base of the simulation
		Car* addCar= new Car(carId_,MroadPlan_,roadDB);       //create a car
		carsDB[carId_]=addCar;                                // add the car to the data base of the simulation
		list<Road*>::iterator it=addCar->getRoadPlan().begin();
		if (it != addCar->getRoadPlan().end()){             // checks if the car has roads in the road plan
			(*it)->getCarsInRoad().push_back(addCar);       //adds the car to the list of cars in the first road in the road plan
			addCar->setRoad(*it);                          //update the pointer of the road in the car
		}
	}
	else{
		cout<<"there is another car with the name: "<<carId_<<endl;
	}
}


void AddCarEvent::print(){
	cout<< "the type is: "<<type_<<endl;
	cout<<"the carId is: "<<carId_<<endl;
	cout<<"timeEvent is: "<<timeEvent_<<endl;
	cout<<"road plan is: "<<MroadPlan_<<endl;

 }


//destructor
AddCarEvent::~AddCarEvent(){
}



//constructor
CarFaultEvent::CarFaultEvent(string type, int timeEvent, string carId, map<string,Car*> & car_DB,int MtimeOfFault):Event(type,carId,timeEvent,car_DB), MtimeOfFault_(MtimeOfFault)
{
}


//This method update a car to be fault
void CarFaultEvent::performEvent(){
	if (carsDB.find(carId_) != carsDB.end()){                     //checks if the car is in the data base of the simulation
		(*carsDB[carId_]).updateFaultyTime(MtimeOfFault_);           // set the car timer of fault
	}else cout<<"there is no car with the name: "<<carId_<<endl;

}

void CarFaultEvent::print(){
	cout<< "the type is: "<<type_<<endl;
	cout<<"the carId is: "<<carId_<<endl;
	cout<<"timeEvent is: "<<timeEvent_<<endl;
	cout<<"time of fault is: "<<MtimeOfFault_<<endl;
}


//destructor
CarFaultEvent::~CarFaultEvent(){
}


bool sortByTime (const Event* event1, const Event* event2)
	{
		return ((event1->getTimeEvent()) < (event2->getTimeEvent()));
	}
