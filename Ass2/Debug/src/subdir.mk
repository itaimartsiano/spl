################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Commands.cpp \
../src/Ini.cpp \
../src/Run.cpp \
../src/Simulator.cpp \
../src/SystemEvents.cpp \
../src/mainObjects.cpp 

OBJS += \
./src/Commands.o \
./src/Ini.o \
./src/Run.o \
./src/Simulator.o \
./src/SystemEvents.o \
./src/mainObjects.o 

CPP_DEPS += \
./src/Commands.d \
./src/Ini.d \
./src/Run.d \
./src/Simulator.d \
./src/SystemEvents.d \
./src/mainObjects.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I/usr/include/boost/property_tree -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


