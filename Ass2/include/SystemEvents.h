#ifndef SYSTEM_EVENTS_H_
#define SYSTEM_EVENTS_H_

#include <iostream>
#include <map>


using namespace std;
class Car;
class Road;

class Event
{
	protected:
        string type_;
        string carId_;
        int timeEvent_ ;
        map<string,Car*> & carsDB;    //Reference to the data base of cars

    public:
        Event (string type,string Id, int timeof,map<string,Car*> & car_DB);
        virtual ~Event();

        //getters
        string getType();
        string getCarId();
        int getTimeEvent() const;

		//methods
        virtual void performEvent()=0;
        virtual void print()=0;

};



class AddCarEvent: public Event
{
    private:
        string MroadPlan_;
        map <string,Road*> & roadDB;        //Reference to the data base of roads

    public:
        AddCarEvent (string planRoad,string type,string Id,int Mtime,map<string,Car*> & car_DB,map <string,Road*> & road_DB);
        virtual ~AddCarEvent();

        //methods
        void performEvent();
        void print();

};



class CarFaultEvent: public Event
{
	private:
	 	 int MtimeOfFault_;

	public:
		 CarFaultEvent(string type, int timeEvent, string carId, map<string,Car*> & car_DB,int MtimeOfFault);
		 virtual ~CarFaultEvent();

		 //methods
		 void performEvent();
		 void print();
};



bool sortByTime (const Event* event1, const Event* event2);

#endif
