#ifndef INI_H_
#define INI_H_

#include <iostream>
#include <map>
#include <list>

using namespace std;
class Road;
class Event;
class Commands;
class Car;
class Junction;

class readerClass
{
	public:
		readerClass();
		virtual ~readerClass();

		//methods
		void readRoadMap(map <string,Road*> & roadDB, map <string,Junction*> & junctionDB,int & max_speed,int defaultTimeSlice) ;
		void readConfig(int & DefaultTime,int & MaxTime, int & MinTime,int & MainSpeed) ;
		void readEvents(list <Event*> & eventList, map <string, Car*> & carDB, map <string, Road*> & roadDB, map <string, Junction*> & junctionDB) ;
		void readCommands(list <Commands*> & commandList, map <string, Car*> & carDB, map <string, Road*> & roadDB, map <string, Junction*> & junctionDB);
};

#endif /* INI_H_ */
