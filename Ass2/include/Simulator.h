
#ifndef SIMULATOR_H_
#define SIMULATOR_H_

class rederClass;
class Car;
class Junction;
class Road;
class Commands;
class Event;

using namespace std;

class Simulator
{
	private:
		readerClass reader;
		map <string,Road*>  roadDB;
		map <string,Car*>  carDB;
		map <string,Junction*>  junctionDB;
		list <Commands*>  commandsDB;
		list <Event*>  EventDB;
		int defaultTimeSlice,maxTime, minTime, maxSpeed ;
		int clock;

	public:
		Simulator();
		virtual ~Simulator();

		//methods
		void initialize();
		bool initialize_Succeed();
		void executeEvents();
		void updateCarHistory();
		void executeCommands();
		void advanceCarsInRoads();
		void advanceCarsInJunctions();
		void Run();

};

#endif
