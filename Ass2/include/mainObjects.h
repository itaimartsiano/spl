#ifndef MAIN_OBJECTS_H_
#define MAIN_OBJECTS_H_

#include <iostream>
#include <vector>
#include <map>
#include <list>

using namespace std;
class Car;
class Junction;

class Road
{
	private:
		string start_junction;
		int length_;
		int & MAX_SPEED;            //Reference to the configuration parameter max speed
		string id_;
		int lastTimeUpdate_;
		int speedRoad_;
		int timeSliceOpen_;			//how much time unit should this road have green light
		int greenLightCounter_;     //counts the time thats left for the time slice
		int carsPassesCounter_;    //count how many cars pass the end junction in one slice time
		list <Car*> carInRoad_;    //list of cars that are currently on the road sorted by location

	public:
		Road (string startJunc,string endJunc,int lengthRoad,int & max_Speed );
		virtual ~Road();

		// getters
		string getStartJunction();
		int getLength();
		string getId();
		int getTheLastTimeOfUpdate();
		int getRoadSpeed();
		int getTimeSliceToOpenLight();
		int getGreenLightCounter();
		int getCarsPassesCounter();
		list<Car*> & getCarsInRoad();

		//setters
		void setGreenLightCounter(int greenLightTime );
		void setTimeSliceToOpen(int timeSlice);
		void setTheLastTimeOfUpdate(int updateTime);
		void setCarsPassesCounter(int counter);
		void setLength(int roadLength);

		//Methods
		void printRoad();
		string queueForJunction();
		string timeSliceForJunction();
		void advanceCars();

};




class Car
{
	private:
		string id_;
		list <Road*> roadPlan;   //list of pointers to roads according to the road plan of the car
		int faultyTime_;
		int speed_;
		int location_;
		string history_;
		Road*  road;             //pointer to the current road that the car is on it

	public:
		Car (string id, string strRoadPlan,  map <string, Road*> & roadBase);
		Car (const Car& newCar);
		virtual ~Car();

		//getters
		string getCarId();
		list<Road*> & getRoadPlan();
		int getFaultyTime();
		int getCarSpeed();
		int getCarLocation() const;
		string getCarHistory();
		Road* getRoad();

		//setters
		void updateFaultyTime(int faultTime);
		void setCarSpeed(int carSpeed);
		void setLocation(int location) ;
		void setRoad(Road* newRoad);

		//methods
		Car & copy(const Car& car);
		Car & operator=(const Car& theCar);
		list <Road*> RoadPlanConverter (string strRoadPlan, map <string, Road*> & roadBase);
		void printCar();
		string getTheNameAndLocation();
		void updateMyHistory(int time);
		void changeRoad();
};



class Junction
{
	private:
		string id_;
		int lightIndex_;                      // which road has green light (the index of that road in the vector)
		vector <Road*> incomingRoads_;        // vector of pointers to the incoming roads

	public:
		Junction (string name);
		virtual ~Junction();

		//getters
		string getJuncId();
		int getLightIndex();
		vector<Road*> & getIncomingRoads();

		//setters
		void setId(string newId);
		void setLightIndex(int newIndex);

		//methods
		void insertRoad(string roadId,map <string, Road*>& roadMap, int defaultTimeSlice);
		void printJuction();
};

bool sortByLocation (const Car* car1, const Car* car2);

#endif
