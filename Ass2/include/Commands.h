
#ifndef COMMAND_H_
#define COMMAND_H_

#include <iostream>
#include <map>

class Car;
class Road;
class Junction;
using namespace std;


class Commands
{
	protected:
		string type_;
		int time_;

	public:
		Commands (string type,int time);
		virtual ~Commands();

		//getters
		string getType();
		int getTimeCommand() const;

		//methods
		virtual void performCommand()=0;
		virtual void printCommand()=0;
};



class Termination:public Commands
{
	public:
		Termination(string type,int time);
		virtual ~Termination();

		//methods
		void printCommand();
		void performCommand();
};



class Report:public Commands
{
	protected:
		string id_;

	public:
		Report(string type,int time,string id);
		virtual ~Report();

		//methods
		virtual void writeReport()=0;
		void performCommand()=0;
};



class CarReport: public Report
{
	private:
		string car_id;
		map<string,Car*> & carsDB;           //Reference to the cars data base

	public:
		CarReport(string type,int time,string id,string carId,map<string,Car*> & cars_DB);
		virtual ~CarReport();

		//methods
		void writeReport();
		void printCommand();
		void performCommand();
};



class RoadReport:public Report
{
	private:
		string start_junction;
		string end_junction;
		map <string,Road*> & roadDB;       //Reference to the roads data base

	public:
		RoadReport(string type,int time,string id,string startJunc,string endJunc, map <string,Road*> & road_DB);
		virtual ~RoadReport();

		//methods
		void writeReport();
		void printCommand();
		void performCommand();
};



class JunctionReport: public Report
{
	private:
		string junction_id;
		map <string,Junction*> & junctionDB;           //Reference to the junctions data base

	public:
		JunctionReport(string type,int time,string id,string juncId, map <string, Junction*> & junction_DB);
		virtual ~JunctionReport();

		//methods
		void writeReport();
		void printCommand();
		void performCommand();
};

bool sortByTimeCommand (const Commands* command1, const Commands* command2);

#endif /* COMMAND_H_ */
