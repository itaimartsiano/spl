//Note: WE HAVE NOT IMPLEMENTED IN THIS
//CLASS A COPY CONSTRUCTOR AND OPERATOR=
#ifndef __POLYGON_H
#define __POLYGON_H

#include <vector>
#include <iostream>
using namespace std;
class Point;

class Polygon {
public:
	/**
	 *default constructor
	 */
	Polygon();
	/**
	 *copy constructor
	 */
	Polygon(int color);

	/*
	 * destructor- notice the "deep" delete
	 */
	virtual ~Polygon();
	/**
	 * adds point p to _points
	 * @param: p the point to be added
	 */
	void addPoint(Point* p);
	/**
	 * @return a pointer to the point at _points(index)
	 */
	Point* getPoint(int index);
	/**
	 * @return the size of _points
	 */

	virtual int getColor()=0;

	virtual string getType()=0;

	int getNumOfPoints();

    Polygon & operator=(const Polygon &P);


private:
	/**
	 * the actual vector the points are held in.
	 */
	std::vector<Point*> _points;

    const int color;

    std::string type;
};


class Rectangle: public Polygon {
	public:
	Rectangle();
	Rectangle(int color_);
	~Rectangle();
	int getColor();
	string getType();


};

class Triangle: Polygon {
	public:
	Triangle();
	Triangle(int color_);
	~Triangle();
	int getColor();
	string getType();


};








#endif
