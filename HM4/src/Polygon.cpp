#include "../include/Point.h"
#include "../include/Polygon.h"
#include <iostream>


using std::cout;
using std::endl;
using std::vector;
using namespace std;


Polygon::Polygon():color(0) {

}

Polygon::Polygon(int color): color(color)
{
	bool stop = true;

	while (stop) {
		cout<<"enter X value point"<<endl;
		double x;
		cin>>x;
		cout<<"enter Y value point"<<endl;
		double y;
		cin>>y;
		addPoint(new Point(x,y));
		cout<<"do you have any more points to enter, true or false?"<<endl;
		cin>>stop;
	}
	if (getNumOfPoints()==2) type = "line";
	if (getNumOfPoints()==3) type = "triangle";
	if (getNumOfPoints()==4) type = "square";
	if (getNumOfPoints()==5) type = "pentagram";
	if (getNumOfPoints()==6) type = "hexagon";
	if (getNumOfPoints()>6) type = "unknown";

}

Polygon::~Polygon() {
	vector<Point*>::iterator iter = _points.begin();
	cout << "DELETING POLYGON: BEGIN" << endl;
	while (iter != _points.end()) {
		cout << "  deleting point: ";
		cout<<(*iter)->getX()<<" ";
		cout<<(*iter)->getY()<<" ";
		cout << endl;
		delete (*iter);
		iter++;
	}
	cout << "DELETING POLYGON: END" << endl;
}

void Polygon::addPoint(Point* p) {
	Point* newp = new Point; //create a copy of the original pt
	newp->setX(p->getX());
	newp->setY(p->getY());
	_points.push_back(newp);
}

Point* Polygon::getPoint(int index) {
	return _points.at(index);
}

int Polygon::getNumOfPoints() {
	return _points.size();
}

int Polygon::getColor() {
	return color;
}

string Polygon::getType() {
	return "polygon";
}

Polygon & Polygon::operator=(const Polygon &P)
{
  // check for "self assignment" and do nothing in that case
  if (this == &P) {
    return *this;
  }else{

  }

  // return this List
  return *this;
}

Triangle::Triangle():Polygon() {

}

Triangle::Triangle(int color_):Polygon(color_){

}
Triangle::~Triangle(){
}

int Triangle::getColor(){
	return (Polygon::getColor());
}


string Triangle::getType(){

	return (Polygon::getType());
}

Rectangle::Rectangle():Polygon() {

}


Rectangle::Rectangle(int color_):Polygon(color_){

}


int Rectangle::getColor(){
	return (Polygon::getColor());
}

Rectangle::~Rectangle(){
}


string Rectangle::getType(){

	return (Polygon::getType());
}












