
public class BadInputFormatException extends Exception{

	public BadInputFormatException(String msg){
		super(msg);
	}
	
	public String getMessage(){
		
		return ("BadInputFormatException:"+super.getMessage());
		
	}

}
