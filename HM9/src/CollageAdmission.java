import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class CollageAdmission {

	public static void checkCandidates(String filename) throws NumberFormatException, IOException, BadInputFormatException{
		
	    BufferedReader inputFile = null;
	    try{
		inputFile = new BufferedReader(new FileReader(filename));
	
	    int PASSING_GRADE = 400; 
        String currentLine;
                
        while ((currentLine = inputFile.readLine())!= null){
            if (!currentLine.matches("(([a-z]|[A-Z])+.?\\s*)+;[0-9]{1,3}")){
            	throw new BadInputFormatException("the input file have a syntax problem");
            }
        	String[] candidateDetails = currentLine.split(";");
            System.out.println(candidateDetails[0] 
                    + ":" + candidateDetails[1] +"\t=> "
                    + (Integer.parseInt(candidateDetails[1])>=PASSING_GRADE?
                                        "ACCEPTED":"DENIED"));
        }
	    }catch(NumberFormatException | IOException e){
	    	e.printStackTrace();
	    }finally{
	    	inputFile.close();
	    	System.out.println("the file have been closed");
	    }
		
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	
	public static void main(String[] args){
			
		try {
			checkCandidates("/home/itai/workspace/HM9/BadFormat.txt");
		} catch (NumberFormatException | IOException | BadInputFormatException e) {
			e.printStackTrace();
		}


	}

}
