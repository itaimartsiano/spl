
public class ValueRunnable implements Runnable {

	private final Value value;
	
	public ValueRunnable(Value v){
		value = v;
	}
	
	@Override
	public void run() {
		
		value.printEntry();

	}

}
