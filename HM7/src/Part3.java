

public class Part3 implements Runnable{
	private final int ID;
	
	public Part3(int id){
		ID = id;
	}

	@Override
	public void run() {
		try {
			System.out.println("the Thread: "+ID+" is going to sleep");
			Thread.sleep(ID*5000);
		} catch (InterruptedException e) {
		}
		System.out.println("the Thread: "+ID+" is waking up");
		
	}



}
