import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Main {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		
		//second Part
		Value value = new Value();
		ExecutorService executor = Executors.newFixedThreadPool(2);

		for (int i=0; i<10; i++){
			ValueRunnable r1 = new ValueRunnable(value);
			executor.submit(r1);
		}
		executor.shutdown();
		executor.awaitTermination(100, TimeUnit.SECONDS);
		
		//third Part
		ScheduledExecutorService scheduler =
			     Executors.newScheduledThreadPool(100);
		
		for (int i=1; i<=100; i++){
			Part3 r1 = new Part3(i);
			scheduler.schedule(r1 , i*10, TimeUnit.SECONDS);
		}
		scheduler.awaitTermination(10,TimeUnit.MINUTES);
		scheduler.shutdownNow();
		

	}

}
