package user;
import bgu.reit.Management;


public class Driver {

	
	public static void main(String[] args) {

		Management management = new Management();
		String InitialDataFile = args[0];
		String AssetContentsRepairDetailsFile = args[1];
		String AssetsFile = args[2];
		String CustomersGroups = args[3];
		XmlParser.assetsParser(AssetsFile, management);
		XmlParser.customersGroupsParser(CustomersGroups, management);
		XmlParser.initialDataParser(InitialDataFile, management);
		XmlParser.assetContentsRepairDetailsParser(AssetContentsRepairDetailsFile, management);
		management.simulation();

	}

}
