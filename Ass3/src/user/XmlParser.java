package user;

import java.io.File;
import java.util.Vector;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import bgu.reit.Management;



public class XmlParser {
	
	private final static Logger LOGGER = Logger.getLogger(XmlParser.class.getName());
	
	public XmlParser(){
	}
	
	
	
	/**
	 * this function get file, parse it and return a Node List of elements
	 * @param element - which main elements to look for
	 * @param fileLocation - file location
	 * @return- node list of elements
	 */
	private static NodeList parseAndGetNodesListOfElements(String element, String fileLocation){
		File xmlFile = new File(fileLocation);
		DocumentBuilder documentBuilder;
		Document document = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			document = documentBuilder.parse(xmlFile);
			document.getDocumentElement().normalize(); //normalize the text nodes
		} catch (Exception e) {
			e.printStackTrace();
		}
		NodeList elementList = document.getElementsByTagName(element);
		return elementList;
	}
	
	
	/**
	 * this function handled the file Asset.xml
	 * @param fileLocation - file location
	 * @param management - the management object - the place to put some data
	 */
	public static void assetsParser(String fileLocation, Management management){
		
		NodeList assetList = parseAndGetNodesListOfElements("Asset",fileLocation);
		for (int i = 0; i < assetList.getLength(); i++) {
			Node tempAssetNode = assetList.item(i);
			if (tempAssetNode.getNodeType() == Node.ELEMENT_NODE) {

				Element assetElement = (Element) tempAssetNode;

				String assetName = assetElement.getElementsByTagName("Name").item(0).getTextContent();
				String assetType = assetElement.getElementsByTagName("Type").item(0).getTextContent();
				int assetSize = Integer.parseInt(assetElement.getElementsByTagName("Size").item(0).getTextContent());
				
				NodeList locationNodes= assetElement.getElementsByTagName("Location");
				Element locationElement = (Element)locationNodes.item(0);
				double assetLocationXValue = Double.parseDouble( locationElement.getAttribute("x"));
				double assetLocationYValue = Double.parseDouble( locationElement.getAttribute("y"));
				double assetCostPerNight = Double.parseDouble(assetElement.getElementsByTagName("CostPerNight").item(0).getTextContent());

				NodeList assetContentList = assetElement.getElementsByTagName("AssetContent");
				Vector <String> assetContentName = new Vector <String>();
				Vector <Double> assetContentRepairMultiplier = new Vector <Double>();
				for (int j = 0; j < assetContentList.getLength(); j++) {
					Node tempAssetContentNode = assetContentList.item(j);
					if (tempAssetContentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element assetContentElement = (Element) tempAssetContentNode;
						assetContentName.add(assetContentElement.getElementsByTagName("Name").item(0).getTextContent());
						assetContentRepairMultiplier.add(Double.parseDouble(assetContentElement.getElementsByTagName("RepairMultiplier").item(0).getTextContent()));
					}
				}
				management.addAsset(assetName,assetType,assetLocationXValue, assetLocationYValue,assetCostPerNight, assetSize,assetContentName,assetContentRepairMultiplier);
			}
		}
		LOGGER.info("finished Assets Parsing");
	} //assetsParser
	
	
	/**
	 * This method parse the xml file CustomersGroups and adds the groups to management
	 * @param fileLocation- the location of the  file we want to read 
	 */
	public static void customersGroupsParser(String fileLocation, Management management){

		NodeList customerGroupList = parseAndGetNodesListOfElements("CustomerGroupDetails", fileLocation);
		for (int i = 0; i < customerGroupList.getLength(); i++) {
			Node tempCustomerGroupNode = customerGroupList.item(i);
			if (tempCustomerGroupNode.getNodeType() == Node.ELEMENT_NODE) {
				Element customerGroupElement = (Element) tempCustomerGroupNode;
			
				//making CustomerGroupDetails
				String groupManagerName = customerGroupElement.getElementsByTagName("GroupManagerName").item(0).getTextContent();
				management.addNewCustomerGroup(groupManagerName);
				
				//adding the customers into the CustomerGroupDetails
				NodeList customersList = customerGroupElement.getElementsByTagName("Customer");
				addCustomerToGroup(management, customersList, groupManagerName);
				
				//adding the rentalRequest into the CustomerGroupDetails
				NodeList rentalRequestsList = customerGroupElement.getElementsByTagName("Request");
				addRentalRequestToGroup(management, rentalRequestsList, groupManagerName);
			}
		}
		LOGGER.info("finished Customers Groups Parsing");
	} //customersGroupsParser
	
	
	/**
	 * this function is adding rental request to specified group
	 * @param management
	 * @param rentalRequestsList - list of rental requests of this asset
	 * @param customerGroupDetails - the group of the customer
	 */
	private static void addRentalRequestToGroup(Management management, NodeList rentalRequestsList,  String groupManagerName){
		for (int k = 0; k < rentalRequestsList.getLength(); k++) {
			Node tempRentalRequestNode = rentalRequestsList.item(k);
			if (tempRentalRequestNode.getNodeType() == Node.ELEMENT_NODE) {
				Element rentalRequestElement = (Element) tempRentalRequestNode;
				String rentalRequestId = rentalRequestElement.getAttribute("id");
				String rentalRequestType = rentalRequestElement.getElementsByTagName("Type").item(0).getTextContent();
				int rentalRequestSize = Integer.parseInt(rentalRequestElement.getElementsByTagName("Size").item(0).getTextContent());
				double  rentalRequestDuration = Double.parseDouble(rentalRequestElement.getElementsByTagName("Duration").item(0).getTextContent());
				management.addRentalRequestToGroup(groupManagerName,rentalRequestId,rentalRequestType,rentalRequestSize,rentalRequestDuration,null);
			}
		}
	}	
	
	
	/**
	 * this function add Customer to specified group
	 * @param management
	 * @param customersList - list of customers in the customer group details
	 * @param customerGroupDetails - the group of the customer
	 */
	private static void addCustomerToGroup(Management management, NodeList customersList, String groupManagerName){
		for (int j = 0; j < customersList.getLength(); j++) {
			Node tempCustomerNode = customersList.item(j);
			if (tempCustomerNode.getNodeType() == Node.ELEMENT_NODE) {
				Element customerElement = (Element) tempCustomerNode;
				String customerName = customerElement.getElementsByTagName("Name").item(0).getTextContent();
				String customerVandalism = customerElement.getElementsByTagName("Vandalism").item(0).getTextContent();
				double  customerMinimumDamage = Double.parseDouble(customerElement.getElementsByTagName("MinimumDamage").item(0).getTextContent());
				double  customerMaximumDamage = Double.parseDouble(customerElement.getElementsByTagName("MaximumDamage").item(0).getTextContent());
	
				management.addCustomerToGroup(groupManagerName,customerName,customerVandalism,customerMinimumDamage,customerMaximumDamage);
			}
		}
	}	
	
	
	/**
	 * this function handled parsing of asset content repair details is needed
	 * @param fileLocation
	 * @param management
	 */
	public static void assetContentsRepairDetailsParser(String fileLocation, Management management){

			NodeList assetContentList = parseAndGetNodesListOfElements("AssetContent", fileLocation);
			for (int i = 0; i < assetContentList.getLength(); i++) {
				Node tempAssetNode = assetContentList.item(i);
				if (tempAssetNode.getNodeType() == Node.ELEMENT_NODE) {

					Element assetContentElement = (Element) tempAssetNode;
					String assetContentName = assetContentElement.getElementsByTagName("Name").item(0).getTextContent();
					
					NodeList toolList = assetContentElement.getElementsByTagName("Tool");								//get list of tool nodes
					Vector<String> toolNameVactor = new Vector<String>();
					Vector<Integer> toolQuantityVector = new Vector<Integer>(); 
					
					//insert tool name and quantity into vectors 
					insertToolNameAndQuantityIntoVectors (toolList, toolNameVactor, toolQuantityVector);
					
					NodeList materialList = assetContentElement.getElementsByTagName("Material");						//get list of tool nodes
					Vector<String> materialNameVactor = new Vector<String>();
					Vector<Integer> materialQuantityVector = new Vector<Integer>(); 
					
					//insert material name and quantity into vectors 
					insertMaterialNameAndQuantityIntoVectors (materialList, materialNameVactor, materialQuantityVector);
					management.addRepairMaterialInformation(assetContentName,materialNameVactor, materialQuantityVector);
					management.addRepairToolInformation(assetContentName, toolNameVactor, toolQuantityVector);
				}
			}
			LOGGER.info("finished Asset Contenet Repair Details Parsing");
	} //AssetContentsRepairDetailsParser
	
	
	
	/**
	 * this function insert tool name to tool name vector and it quantity to quantity vector
	 * @param toolList - list of tools
	 * @param toolNameVactor - vector of name
	 * @param toolQuantityVector - vector of quantity
	 */
	private static void insertToolNameAndQuantityIntoVectors (NodeList toolList, Vector<String> toolNameVactor, Vector<Integer> toolQuantityVector){
		for (int j = 0; j < toolList.getLength(); j++) {
			Node tempToolNode = toolList.item(j);
			if (tempToolNode.getNodeType() == Node.ELEMENT_NODE) {
				Element toolElement = (Element) tempToolNode;
				String toolName = toolElement.getElementsByTagName("Name").item(0).getTextContent();
				toolNameVactor.add(toolName);
				int  toolQuantity = Integer.parseInt(toolElement.getElementsByTagName("Quantity").item(0).getTextContent());
				toolQuantityVector.add(toolQuantity); 
			}
		}
	}
	
	
	/**
	 * this function insert material name to material name vector and it quantity to quantity vector
	 * @param materialList- list of materials
	 * @param materialNameVactor - vector of materials name
	 * @param materialQuantityVector - vector of quantity 
	 */
	private static void insertMaterialNameAndQuantityIntoVectors (NodeList materialList, Vector<String> materialNameVactor, Vector<Integer> materialQuantityVector){
	for (int k = 0; k < materialList.getLength(); k++) {

		Node tempMaterialNode = materialList.item(k);

		if (tempMaterialNode.getNodeType() == Node.ELEMENT_NODE) {

			Element materialElement = (Element) tempMaterialNode;
			String materialName = materialElement.getElementsByTagName("Name").item(0).getTextContent();
			materialNameVactor.add(materialName);
			int  materialQuantity = Integer.parseInt(materialElement.getElementsByTagName("Quantity").item(0).getTextContent());
			materialQuantityVector.add(materialQuantity);
		}
	}
	}
	
	
	/**
	 * this function handled initial data file, add all the repair tools, repair materials, clerks to management
	 * @param fileLocation
	 * @param management
	 */
	public static void initialDataParser(String fileLocation, Management management){

			NodeList toolList = parseAndGetNodesListOfElements("Tool", fileLocation);
				
			addRepairToolsToManagement(toolList, management);
			
			NodeList materialList = parseAndGetNodesListOfElements("Material", fileLocation);	
			addMaterialsToManagement(materialList, management);
			
			NodeList clerkList = parseAndGetNodesListOfElements("Clerk", fileLocation);	
			addClerkToManagement(clerkList, management);
			
			int  maintenancePersons = Integer.parseInt(parseAndGetNodesListOfElements("NumberOfMaintenancePersons", fileLocation).item(0).getTextContent());
			management.updateNumberOfMaintenancePersons(maintenancePersons);
			
			int  NumberOfRentalRequests = Integer.parseInt(parseAndGetNodesListOfElements("TotalNumberOfRentalRequests", fileLocation).item(0).getTextContent());
			management.updateNumberOfRentalRequests(NumberOfRentalRequests);	
			
			LOGGER.info("finished Initial Data Parsing");
		
	} //initialDataParser
	
	
	/**
	 * this function add repair tools to management
	 * @param toolList - node list of repair tools
	 * @param management
	 */
	private static void addRepairToolsToManagement(NodeList toolList, Management management){
		for (int j = 0; j < toolList.getLength(); j++) {
			
			Node tempToolNode = toolList.item(j);

			if (tempToolNode.getNodeType() == Node.ELEMENT_NODE) {

				Element toolElement = (Element) tempToolNode;
				String toolName = toolElement.getElementsByTagName("Name").item(0).getTextContent();
				int  toolQuantity= Integer.parseInt(toolElement.getElementsByTagName("Quantity").item(0).getTextContent());
				management.addItemRepairTool(toolName, toolQuantity);
			}
		}
	}
	
	
	/**
	 * this function add materials to management
	 * @param materialList - node list of materials
	 * @param management
	 */
	private static void addMaterialsToManagement(NodeList materialList, Management management){
		for (int i = 0; i < materialList.getLength(); i++) {
			
			Node tempMaterialNode = materialList.item(i);

			if (tempMaterialNode.getNodeType() == Node.ELEMENT_NODE) {

				Element materialElement = (Element) tempMaterialNode;
				String materialName = materialElement.getElementsByTagName("Name").item(0).getTextContent();
				int  materialQuantity= Integer.parseInt(materialElement.getElementsByTagName("Quantity").item(0).getTextContent());
				management.addItemRepairMaterials(materialName, materialQuantity);
			}
		}
	}
	
	
	/**
	 * this function add the clerk to management
	 * @param clerkList - node list of ckerks
	 * @param management
	 */
	private static void addClerkToManagement(NodeList clerkList, Management management){
		for (int k = 0; k < clerkList.getLength(); k++) {
			
			Node tempClerkNode = clerkList.item(k);
	
			if (tempClerkNode.getNodeType() == Node.ELEMENT_NODE) {
				
				Element clerkElement = (Element) tempClerkNode;
				String clerkName = clerkElement.getElementsByTagName("Name").item(0).getTextContent();
				NodeList locationNodes= clerkElement.getElementsByTagName("Location");
				Element locationElement = (Element)locationNodes.item(0);
				double clerkLocationXValue = Double.parseDouble( locationElement.getAttribute("x"));
				double clerkLocationYValue = Double.parseDouble( locationElement.getAttribute("y"));
				management.addClerk(clerkName, clerkLocationXValue, clerkLocationYValue);
			}
		}
	}
	
} 
