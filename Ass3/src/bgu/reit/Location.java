package bgu.reit;


class Location {

	private final double fx;
	private final double fy;


	/**
	 * constructor of the location
	 * @param x - x value of the location
	 * @param y - y value of the location
	 */
	public Location(double x, double y){
		this.fx = x;
		this.fy = y;
	}


	/**
	 * calculate the distance between 2 Location objects
	 * @param other is the Location object which we measure the distance to
	 * @return	the distance between the 2 Location objects
	 */
	public double calculateDistance(Location other){
		return 		Math.sqrt(Math.abs(Math.pow((this.fx-other.fx), 2)+Math.pow((this.fy-other.fy),2)));
	}

}
