package bgu.reit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;


public class Management {
	private ArrayList<ClerkDetails> fClerkDetails;
	private ArrayList<CustomerGroupDetails> fCustomerGroupDetailsDS;
	private Assets fAssets;
	private Warehouse fWarehouse;
	private Map<String,List<RepairMaterialInformation>> fRepairMaterialInformation; 
	private Map<String,List<RepairToolInformation>> fRepairToolInformation;			
	private LinkedBlockingQueue<RentalRequest> fQueueOfRentalRequests;
	private Vector<DamageReport> fContainerOfDamageReports;
	private int fTotalNumberRentalRequests;
	private int numberOfMaintenancePersons; 
	private AtomicInteger fNumberOfFulfilledRentalRequests;
	private int fNumberOfDamageReportHandled;
	private CountDownLatch fClerksLatchObject;
	private Object fShiftLock = new Object();
	private Object fDamageReportLocker = new Object();
	private final Logger LOGGER = Logger.getLogger(Management.class.getName());
	private	Statistics fStatistics; 

	
	/**
	 * Constructor of the management 
	 */
	public Management(){
		fClerksLatchObject = null;
		fClerkDetails = new ArrayList<ClerkDetails>();
		fCustomerGroupDetailsDS = new ArrayList<CustomerGroupDetails>();
		fAssets = new Assets();
		fWarehouse = new Warehouse();
		fRepairMaterialInformation = new HashMap<String,List<RepairMaterialInformation>>();
		fRepairToolInformation = new HashMap<String,List<RepairToolInformation>>();
		fContainerOfDamageReports = new Vector<DamageReport>();
		fQueueOfRentalRequests = new LinkedBlockingQueue<RentalRequest>();
		fNumberOfFulfilledRentalRequests = new AtomicInteger();
		fStatistics = new Statistics();
	}


	/**
	 * This method simulate a real estate investment trust BGU-REIT
	 */
	public void simulation(){
		LOGGER.info("STARTING SIMULATION");
		LOGGER.info("starting with "+fTotalNumberRentalRequests+"rental requests");
		runAllClerks();
		runAllCustomerGroupManagers();
		while (fNumberOfFulfilledRentalRequests.get() < fTotalNumberRentalRequests){
			try {
				fClerksLatchObject.await();
			} catch (InterruptedException exception) {
				LOGGER.severe("management was inturapted while waiting on ClerksLatchObject ");
				exception.printStackTrace();
			}
			reset(Math.min(fClerkDetails.size(),(fTotalNumberRentalRequests-fNumberOfFulfilledRentalRequests.get())));
			LOGGER.info("all the clerks finish their shift starting to handle all the damage reports");
			handleDamageReports();
			List <Asset> damagedAssets = fAssets.getDamagedAssets();
			LOGGER.info("finish handle all the damage reports starting to fix the damaged assets");
			handleMaintenenceRequests(damagedAssets);
			synchronized(fShiftLock){ //waking all the clerks for a new shift
				LOGGER.info("waking all the clerks for a new shift");
				fShiftLock.notifyAll();
			}
		}
		LOGGER.info("SIMULATION ENDED");
		LOGGER.info("the statistics are: " + fStatistics.toString());
	}


	/**
	 * run over the fClerkDetails array list, make RunableClerk for every clerk, run it as a Thread
	 */
	private void runAllClerks(){
		LOGGER.info("Launch all clerks");
		fClerksLatchObject = new CountDownLatch(fClerkDetails.size());
		for (int i=0; i<fClerkDetails.size(); i++){
			RunnableClerk clerk = new RunnableClerk(fQueueOfRentalRequests, fClerkDetails.get(i), fNumberOfFulfilledRentalRequests,
					this, fTotalNumberRentalRequests,fShiftLock);
			Thread thread = new Thread(clerk);
			thread.start();
		}
	}


	/**
	 * run over the fCustomerGroupDetailsDS array list, make RunnableCustomerGroupManager for every CustomerGroupDetails, run it as a Thread
	 */
	private void runAllCustomerGroupManagers(){
		LOGGER.info("Launch all customers group manager");
		for (int i=0; i<fCustomerGroupDetailsDS.size(); i++){
			RunnableCustomerGroupManager customerGroupManager = new RunnableCustomerGroupManager(fCustomerGroupDetailsDS.get(i),this,fStatistics);
			Thread thread = new Thread (customerGroupManager);
			thread.start();
		}
	}


	/**
	 * for every damage report updates the health of the asset
	 */
	private void handleDamageReports(){
		//this take care of the situation of threads that increase the fNumberOfFulfilledRentalRequests too much
		if (fNumberOfFulfilledRentalRequests.get() > fTotalNumberRentalRequests) fNumberOfFulfilledRentalRequests.set(fTotalNumberRentalRequests);
		
		while (fNumberOfDamageReportHandled != fNumberOfFulfilledRentalRequests.get()){
			synchronized (fDamageReportLocker){
				if (fContainerOfDamageReports.size() == (fNumberOfFulfilledRentalRequests.get()-fNumberOfDamageReportHandled)){
					for (DamageReport currentDamageReport : fContainerOfDamageReports){
						double assetDamage = currentDamageReport.getAssetDamage();
						Asset currentAsset = currentDamageReport.getAsset();
						currentAsset.updateHealthAfterStay(assetDamage);
					}
					fContainerOfDamageReports.clear();
					fNumberOfDamageReportHandled = fNumberOfFulfilledRentalRequests.get();
				}else{
					try {

						fDamageReportLocker.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}
	}

	
	/**
	 * This method is responsible for fixing all the damaged assets according to the damage reports 
	 */
	private void handleMaintenenceRequests(List <Asset> damagedAssets){
		
		ExecutorService MaintenencePersonExecutor = Executors.newFixedThreadPool(numberOfMaintenancePersons);
		LOGGER.info("creating all the maintenence requests");
		
		for (int i = 0; i < damagedAssets.size();i++) {
			Runnable maintenenceRequest = new RunnableMaintenanceRequests(fRepairToolInformation, fRepairMaterialInformation,damagedAssets.get(i),fWarehouse , fStatistics);
			MaintenencePersonExecutor.execute(maintenenceRequest);
		}
		MaintenencePersonExecutor.shutdown();
		try {
			MaintenencePersonExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
			LOGGER.info("all the assets are fixed");
			damagedAssets.clear();
		} catch (InterruptedException e) {
			LOGGER.severe("MaintenencePersonExecutor was interupted");
		}
	}


	/**
	 * This method creates and adds a clerk to the data structure of clerk details
	 * @param clerkName - name of clerk
	 * @param clerkLocationXValue - the x value of the location of the clerk
	 * @param clerkLocationYValue - the y value of the location of the clerk
	 */
	public void addClerk(String clerkName,double clerkLocationXValue,double clerkLocationYValue){
		Location location = new Location( clerkLocationXValue,clerkLocationYValue);
		ClerkDetails clerk = new ClerkDetails(clerkName, location);
		fClerkDetails.add(clerk);

	}


	/**
	 * This method creates and adds a customer group to the data structure of customer group details
	 * @param managerName - the group manager name
	 */
	public void addNewCustomerGroup(String managerName){
		CustomerGroupDetails customerGroupDetails = new CustomerGroupDetails(managerName);
		fCustomerGroupDetailsDS.add(customerGroupDetails);
	}


	/**
	 * This method creates and adds a customer to a given customer group 
	 * @param customerGroupDetails - the group that the customer will be added to
	 * @param name - name of the customer
	 * @param vandalism - vandalism type of the customer
	 * @param minimumDamage - minimum damage the customer can do
	 * @param maximumDamage - maximum damage the customer can do
	 */
	public void addCustomerToGroup(String managerName, String name,String vandalism, double minimumDamage, double maximumDamage){
		CustomerGroupDetails currentCustomerGroupDetails =null;
		for(int i=0; i<fCustomerGroupDetailsDS.size(); i++){
			if (fCustomerGroupDetailsDS.get(i).getName().equals(managerName)){
				currentCustomerGroupDetails = fCustomerGroupDetailsDS.get(i);
			}
		}
		currentCustomerGroupDetails.addCustomer(new Customer(name,vandalism,minimumDamage,maximumDamage));
		
	}
	

	/**
	 * This method creates and adds a rental request to a given customer group  
	 * @param customerGroupDetails - the group that the rental request will be added to
	 * @param id - id of the rental request
	 * @param assetType - the asset type of the rental request
	 * @param assetSize - the size type of the rental request
	 * @param durationOfStay - the duration of stay in the asset 
	 * @param asset - the asset requested
	 */
	public void addRentalRequestToGroup(String managerName, String id,String assetType,int assetSize,double durationOfStay,Asset asset){
		CustomerGroupDetails currentCustomerGroupDetails =null;
		for(int i=0; i<fCustomerGroupDetailsDS.size(); i++){
			if (fCustomerGroupDetailsDS.get(i).getName().equals(managerName)){
				currentCustomerGroupDetails = fCustomerGroupDetailsDS.get(i);
			}
		}
		currentCustomerGroupDetails.addRentalRequest(new RentalRequest(id,assetType,assetSize,durationOfStay,asset));	
	}


	/**
	 * This method creates and adds an asset to the data structure of assets
	 * @param name - name of the asset
	 * @param type - type of the asset
	 * @param locationXValue - x value of the location of the asset
	 * @param locationYValue - y value of the location of the asset
	 * @param costPerNight - cost per night of the asset
	 * @param size - size of the asset
	 * @param assetContentName - vector of all the asset contents names
	 * @param assetContentRepairCost - vector of all the asset contents repair cost
	 */
	public void addAsset(String name, String type, double locationXValue,double locationYValue, double costPerNight, int size,Vector<String> assetContentName,Vector<Double> assetContentRepairCost){
		Location location = new Location(locationXValue,locationYValue);
		Asset asset = new Asset (name,type,location,costPerNight, size);
		for(int i=0;i<assetContentName.size();i++){
			asset.addAssetContent(assetContentName.get(i), assetContentRepairCost.get(i));
		}
		fAssets.insertAsset(asset);
	}

	
	/**
	 * This method adds a rental request to a queue that the clerks can have access to it
	 * @param newRentalRequest
	 */
	public void addRentalRequestToBlockingQueue(RentalRequest newRentalRequest) {
		fQueueOfRentalRequests.add(newRentalRequest);		
	}
	
	
	/**
	 * This method adds a damage report to the container of the damage reports 
	 * @param damageReport
	 */
	public void addDamageReport(DamageReport damageReport) {
		synchronized (fDamageReportLocker){
			fContainerOfDamageReports.add(damageReport);
			fDamageReportLocker.notifyAll();
		}
	}
	
	
	/**
	 * This method creates and adds a repair tool to the warehouse  
	 * @param name - name of the tool
	 * @param quantity - quantity of the tool
	 */
	public void addItemRepairTool(String name, int quantity){
		fWarehouse.addRepairTool(new RepairTool(name,quantity));
	}


	/**
	 * This method creates and adds a repair material to the warehouse  
	 * @param name - name of the material
	 * @param quantity - quantity of the material
	 */
	public void addItemRepairMaterials(String name, int quantity){
		fWarehouse.addRepairMaterial(new RepairMaterial(name,quantity));
	}


	/**
	 * This method adds a repair tool information to the data structure of repair tool information
	 * @param assetContent - the name of the asset content that the given tool information will be added with
	 * @param listOfRepairTool - the list of all the repair tool needed for the asset content
	 */
	public void addRepairToolInformation(String assetContent, Vector<String> toolName,Vector<Integer> toolQuantity){
		List<RepairToolInformation> listOfRepairTool = makeRepairToolInformationList(toolName,toolQuantity);
		fRepairToolInformation.put(assetContent, listOfRepairTool);
	}
	
	
	/**
	 * This method adds a repair material information to the data structure of repair material information
	 * @param assetContent - the name of the asset content that the given material information will be added with
	 * @param listOfRepairTool - the list of all the repair material needed for the asset content
	 */
	public void addRepairMaterialInformation(String assetContent,Vector<String> materialName,Vector<Integer> materialQuantity){
		List<RepairMaterialInformation> listOfRepairMaterial =  makeRepairMaterialInformationList(materialName,materialQuantity);
		fRepairMaterialInformation.put(assetContent,(List <RepairMaterialInformation> ) listOfRepairMaterial);
	}
	

	/**
	 * This method creates a list of repair tool information
	 * @param toolName - name of the tool
	 * @param toolQuantity - quantity of the tool
	 * @return List<RepairToolInformation>
	 */
	public List<RepairToolInformation> makeRepairToolInformationList(Vector<String> toolName,Vector<Integer> toolQuantity){
		List<RepairToolInformation> listOfRepairToolInformation = new LinkedList<RepairToolInformation>();
		for(int i=0;i<toolName.size();i++){
			listOfRepairToolInformation.add(new RepairToolInformation(toolName.get(i),toolQuantity.get(i)));
		}
		return listOfRepairToolInformation;
	}

	
	/**
	 * This method creates a list of repair material information
	 * @param toolName - name of the material
	 * @param toolQuantity - quantity of the material
	 * @return List<RepairMaterialInformation>
	 */
	public List<RepairMaterialInformation> makeRepairMaterialInformationList(Vector<String> materialName,Vector<Integer> materialQuantity){
		List<RepairMaterialInformation> listOfRepairMaterialInformation = new LinkedList<RepairMaterialInformation>();
		for(int i=0;i<materialName.size();i++){
			listOfRepairMaterialInformation.add(new RepairMaterialInformation(materialName.get(i),materialQuantity.get(i)));
		}
		return listOfRepairMaterialInformation;
	}

	
	/**
	 * This method updates the number of maintenance persons 
	 * @param number - number of maintenance persons
	 */
	public void updateNumberOfMaintenancePersons(int number){
		numberOfMaintenancePersons = number;
	}


	/**
	 * This method updates the number of rental requests 
	 * @param number - number of rental requests
	 */
	public void updateNumberOfRentalRequests(int number){
		fTotalNumberRentalRequests = number;
	}

	
	/**
	 * This method finds a suitable asset for the clerk
	 * @param assetType - the type of asset requested
	 * @param assetSize - the size of asset requested
	 * @return Asset
	 */
	public Asset findSuitableAsset(String assetType, int assetSize) {
		return fAssets.find(assetSize, assetType);
	}

	
	/**
	 * This method inform the treads that are waiting on Assets that a asset is available
	 */
	public void wakeUpSleepOnAssets() {
		synchronized(fAssets){
			fAssets.notifyAll();
		}
	}

	
	/**
	 * This method reset the count down latch of the clerks
	 * @param size
	 */
	private void reset(int size){
		fClerksLatchObject= new CountDownLatch(size);
	}


	/**
	 * This method decrease the countDownLatch when a clerk finished his shift
	 */
	public void countDownLatchObject() {
		this.fClerksLatchObject.countDown();
	}

}
