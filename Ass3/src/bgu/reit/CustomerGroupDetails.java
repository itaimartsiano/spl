package bgu.reit;

import java.util.ArrayList;
import java.util.Vector;


class CustomerGroupDetails {

	private ArrayList<RentalRequest> fContainerOfRentalRequest;
	private Vector<Customer> fContainerOfCustomers;
	private String fGroupManagerName;

	
	/**
	 * constructor of the customer group details
	 * @param groupManagerName -the group manager name
	 */
	public CustomerGroupDetails(String groupManagerName){
		fGroupManagerName=groupManagerName;
		fContainerOfRentalRequest=new ArrayList<RentalRequest>();
		fContainerOfCustomers=new Vector<Customer>();
	}


	/**
	 * getter for all the customers that are in this gruop
	 * @return Vector of Customer
	 */
	public Vector<Customer> getContainerOfCustomers() {
		return fContainerOfCustomers;
	}


	/**
	 * getter for the name of the group
	 * @return String
	 */
	public String getName(){
		return fGroupManagerName;
	}


	/**
	 * getter for the number of rental request that the group contains
	 * @return int
	 */
	public int getNumberOfRentalRequests(){
		return fContainerOfRentalRequest.size();
	}


	/**
	 * getter for a rental request
	 * @return RentalRequest
	 */
	public RentalRequest getRentalRequest() {
		return fContainerOfRentalRequest.remove(0);
	}


	/**
	 * This method add a customer to the group
	 * @param customer - a customer of Customer type
	 */
	public void addCustomer(Customer customer){
		fContainerOfCustomers.add(customer);
	}


	/**
	 * This method add a rantal request to the group
	 * @param rental request - a rental request of RentalRequest type
	 */
	public void addRentalRequest(RentalRequest rentalRequest){
		fContainerOfRentalRequest.add(rentalRequest);
	}

}
