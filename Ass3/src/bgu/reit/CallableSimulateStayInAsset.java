package bgu.reit;

import java.util.concurrent.Callable;

class CallableSimulateStayInAsset implements Callable<Float> {

	private Customer fCustomer;
	private double fDurationOfStay;


	/**
	 * constructor of the simulation of staying in an asset
	 * @param ACustomer -the customer that need to stay in the asset
	 * @param durationOfStay- how long the customer need to stay
	 */

	public CallableSimulateStayInAsset(Customer ACustomer,double durationOfStay){
		fCustomer = ACustomer;
		fDurationOfStay = durationOfStay;
	}


	/**
	 * This method simulate the customer staying in an asset
	 * @return Float- the damage percentage done to the asset
	 */
	public Float call(){
		Float customerDamage = fCustomer.calculateDamage();
		final long DAY_IN_MILISECONDS = 24000; 
		long durationOfStayInSeconds = (long) (fDurationOfStay * DAY_IN_MILISECONDS);
		try {
			Thread.sleep(durationOfStayInSeconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return customerDamage;
	}

}
