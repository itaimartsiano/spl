package bgu.reit;


/**
 *Warehouse is object which contain repair tools and repair materials <P>
 *there is a limited number of tools <P>
 *there is "unlimited" repair material in the warehouse
 * @author itai & Natali
 */

public interface WarehouseInterface {
	
	/**
	 * this function will decrease the number of the asked RepairTool in the Warehouse
	 * @param name  - the name of RepairTool wanted object
	 * @param quantity - how much RepairTool the caller needs
	 * @pre - @param quantity <=  availableRepairTool @param name
	 * @inv - availableRepairTool @param name >=0
	 * @post -  availableRepairTool @param name  = @pre quantity - @param quantity
	 * @post - availableRepairTool @param name>=0
	 * @return - true if he get the amount of RepairTools he needed, otherwise false
	 */
	public boolean acquireRepairTool(String name, int quantity);
	
	
	/**
	 * this function will decrease the number of the asked RepairMaterial
	 * @param name  - the name of RepairMaterial wanted object
	 * @param quantity - how much RepairMaterial the caller needs
	 * @pre - @param quantity <=  availableRepairMaterial @param name
	 * @inv -availableRepairMaterial @param name >=0
	 * @post - availableRepairMaterial @param name >=0
	 * @post - availableRepairMaterial @param name = @pre availableRepairMaterial @param name - @param quantity
	 * @return - true if he get the amount of RepairMaterial he needed, otherwise false
	 */
	public boolean acquireRepairMaterial(String name, int quantity);

	/**
	 * this function will increase the number of the  RepairTool in the warehouse
	 * @param name  - the name of  the released RepairTool
	 * @param quantity - how much RepairTool returned
	 * @pre -	initial quantity of RepairTool @parm name >= availableRepairTool @param name + @param quantity 
	 * @inv -the number of RepairTool is equal or smaller to the initial amount of ReapirTool
	 * @post - availableRepairMaterial @param name = @pre availableRepairMaterial @param name + @param quantity
	 * @post - the number of RepairTool is equal or smaller to the initial amount of ReapirTool
	 * @return - true if the function succeded releasing , otherwise false
	 */
	public boolean releasRepairTool(String name,  int quantity);
	
	
}
