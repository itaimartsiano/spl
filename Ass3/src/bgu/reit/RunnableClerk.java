package bgu.reit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

class RunnableClerk implements Runnable {

	private ClerkDetails  fClerkDetails;
	private final BlockingQueue <RentalRequest> fRentalRequests;
	private AtomicInteger fNumberOfFullfilledRentalRequests;       //number of rental request that are  handled by clerks
	private Management fManagement;
	private final Logger LOGGER = Logger.getLogger(RunnableClerk.class.getName());
	private final int MAX_SHIFT_TIME = 8;
	private final int NUMBER_OF_RENTAL_REQUEST;
	private Object fShiftLock;

	/**
	 * Constructor to RunnableClerk
	 * @param RentalRequestsQueue - the shared resource of RentalRequests to all the Clerks
	 */
	public RunnableClerk (BlockingQueue<RentalRequest> RentalRequestsQueue, ClerkDetails clerk, AtomicInteger numberOfFullfilledRentalRequests,Management management, 
			int numberOfRentalRequests, Object shiftLock){
		fRentalRequests = RentalRequestsQueue;
		fClerkDetails = clerk;
		fNumberOfFullfilledRentalRequests = numberOfFullfilledRentalRequests;
		fManagement = management;
		NUMBER_OF_RENTAL_REQUEST = numberOfRentalRequests;
		fShiftLock = shiftLock;
	}


	/**
	 * this is the run function of runnable object RunnableClerk <P>
	 * the clerk to take care of rental requests 
	 */
	public void run() {
		LOGGER.fine(fClerkDetails.getName()+" started working");
		while (fNumberOfFullfilledRentalRequests.incrementAndGet() <= NUMBER_OF_RENTAL_REQUEST){
			int timeOfShift = 0;
			while (timeOfShift==0 || (timeOfShift <= MAX_SHIFT_TIME && fNumberOfFullfilledRentalRequests.incrementAndGet() <= NUMBER_OF_RENTAL_REQUEST )){	
				try {
					LOGGER.info(fClerkDetails.getName() + " entered a new shift");
					RentalRequest currentRentalRequest = fRentalRequests.take();			//current rental request
					Asset suitableAsset = findSuitableAsset(currentRentalRequest);			//suitable asset for rental request
					currentRentalRequest.setAsset(suitableAsset);
					LOGGER.info(fClerkDetails.getName() + " found suitable asset - " + suitableAsset.getName()+" , for request: " + currentRentalRequest.getId());
					currentRentalRequest.updateRequestStatusToFulfilled();
					final int DO_IT_FOR_TIMES = 2;
					final int SECOND_IN_MILLISECOND = 1000;
					Location suitableAssetLocation = suitableAsset.getLocation();			//location of asset
					double distanceTimeToSleep = suitableAssetLocation.calculateDistance(fClerkDetails.getLocation()) * DO_IT_FOR_TIMES * SECOND_IN_MILLISECOND;
					Thread.sleep(Math.round(distanceTimeToSleep));
					synchronized (currentRentalRequest){ currentRentalRequest.notify();}	//wake up the runnable manager that wait for this rental request
					timeOfShift = (int) (timeOfShift + distanceTimeToSleep);
				} catch (InterruptedException exception) {
					exception.printStackTrace();
				}
				LOGGER.info(fClerkDetails.getName()+" finished a rental request" );
			}
			try {
				synchronized(fShiftLock){	//synchronize this section because maybe between the time you countdown and do the wait command the management will do notify all on this.
					LOGGER.info(fClerkDetails.getName()+ " ended his shift and went to sleep");
					fManagement.countDownLatchObject();
					fShiftLock.wait();
				}
			} catch (InterruptedException exception) {
				LOGGER.severe("RunnableClerk was inturupted");
				exception.printStackTrace();
			}
		}	
		LOGGER.info(fClerkDetails.getName()+ " ---exiting---");
	}


	/**
	 * This method finds a suitable asset for the clerk
	 * @param rentalRequest 
	 * @return Asset
	 */
	private Asset findSuitableAsset(RentalRequest rentalRequest){
		String assetType = rentalRequest.getAssetType();
		int assetSize = rentalRequest.getAssetSize();
		Asset suitableAsset = fManagement.findSuitableAsset(assetType, assetSize);
		return suitableAsset;
	}

}
