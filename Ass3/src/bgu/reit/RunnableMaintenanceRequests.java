package bgu.reit;


import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import bgu.reit.Asset;


class RunnableMaintenanceRequests implements Runnable {

	private final Map <String,List<RepairToolInformation>> fRepairToolInformationDS;
	private final Map <String,List<RepairMaterialInformation>> fRepairMaterialInformationDS;
	private final Asset fAsset;
	private final Warehouse fWarehouse;
	private final Logger LOGGER = Logger.getLogger(RunnableMaintenanceRequests.class.getName());
	private final Statistics fStatistics ;

	
	/**
	 * constructor to RunnableMaintenanceRequests
	 * @param RepairToolInformationDB - the data structure of repair tool information for every asset content
	 * @param RepairMaterialInformation - the data structure of repair material information for every asset content
	 * @param asset - the asset to need to be fix
	 * @param warehouse - the place that include all the tools and materials
	 */
	public RunnableMaintenanceRequests (Map <String,List<RepairToolInformation>> RepairToolInformationDB, Map <String,List<RepairMaterialInformation>> RepairMaterialInformation,
			Asset asset, Warehouse warehouse, Statistics statistics) {
		this.fRepairToolInformationDS = RepairToolInformationDB;
		this.fRepairMaterialInformationDS = RepairMaterialInformation;
		this.fAsset = asset;
		this.fWarehouse = warehouse;
		this.fStatistics = statistics;
	}


	/**
	 * this is the run function RunnableMaintenanceRequests <p>
	 * it uses private methods to acquire tools and materials, sleep the cost time repair and release them back 
	 */
	public void run() {
		LOGGER.fine("started fixing "+fAsset.getName());
		TreeMap <String, Integer> groceryListTool = makeGroceryListTools();
		TreeMap <String, Integer> groceryListMaterial = makeGroceryListMaterials();
		long repairCostTime = Math.round(fAsset.assetRepairCostTime());
		acquireToolsAndMaterials(groceryListTool, groceryListMaterial);
		try {
			Thread.sleep(repairCostTime);
		} catch (InterruptedException exception) {
			LOGGER.severe("the sleep was interuptted");
			exception.printStackTrace();
		}
		releaseTools (groceryListTool);
		fAsset.updateHealthAfterFix();
		LOGGER.info(fAsset.getName() + "  is fixed!");
	}


	/**
	 * This method release all the tools of the  maintenance person
	 * @param groceryListTool - the list of tools to release
	 */
	private void releaseTools (TreeMap <String, Integer> groceryListTool){
		Set <String> ToolsSet = groceryListTool.keySet();
		for(String key: ToolsSet){
			fWarehouse.releasRepairTool(key, groceryListTool.get(key));
		}
		LOGGER.fine("release tools have finished");
	}


	/**
	 * This method  acquire all the grocery list from the warehouse is needed to fix Asset
	 * @param groceryListTool - list of tools in needed to fix the specified asset
	 * @param groceryListMaterial - list of materials in needed to fix the specified asset
	 */
	private void acquireToolsAndMaterials(TreeMap <String, Integer> groceryListTool, TreeMap <String, Integer> groceryListMaterial){
		Set <String> materialSet = groceryListMaterial.keySet();
		for(String key: materialSet){
			fWarehouse.acquireRepairMaterial(key, groceryListMaterial.get(key));
			fStatistics.addRepairMaterial(key, groceryListMaterial.get(key));
		}
		LOGGER.fine("acquiring materials have finished");

		Set <String> ToolsSet = groceryListTool.keySet();
		for(String key: ToolsSet){
			fWarehouse.acquireRepairTool(key, groceryListTool.get(key));
			fStatistics.addRepairTool(key, groceryListTool.get(key));
		}
		LOGGER.fine("acquiring tools have finished");

	}


	/**
	 * This method makes a grocery list of tools which needed in order to fix the asset, it run over all the asset contents, 
	 * for every asset content, he check which repair tools is needed and insert them into a sorted tree map
	 * @return - tree map with keys of strings and Integers values
	 */
	private TreeMap <String, Integer> makeGroceryListTools(){

		TreeMap <String, Integer> groceryListTools = new TreeMap <String, Integer>(); 
		Vector <AssetContent> assetContents = fAsset.getAssetContents();
		Iterator <AssetContent> assetContent = assetContents.iterator();

		while (assetContent.hasNext()){										//run over all contents in the house
			String nameOfAssetContent = assetContent.next().getName();
			List<RepairToolInformation> ListofRepairToolsNeeded = fRepairToolInformationDS.get(nameOfAssetContent);
			Iterator <RepairToolInformation> repairTool = ListofRepairToolsNeeded.iterator();

			while (repairTool.hasNext()){									//run over all repair tools needed to fix the specified tool
				RepairToolInformation currentRepairTool =  repairTool.next();
				if (groceryListTools.containsKey(currentRepairTool.getName())){
					if (groceryListTools.get(currentRepairTool.getName()).intValue() < currentRepairTool.getQuantity()){
						groceryListTools.put(currentRepairTool.getName(), currentRepairTool.getQuantity());
					}
				}else{
					groceryListTools.put(currentRepairTool.getName(), currentRepairTool.getQuantity());   
				}
			} 
		}
		LOGGER.finest("making Grocery List Tools is done");
		return groceryListTools;
	}


	/**
	 * This method makes a grocery list of materials which needed in order to fix the asset, it run over all the asset contents, 
	 * for every asset content, he check which repair materials is needed and insert them into a sorted tree map
	 * @return - tree map with keys of strings and Integers values
	 */
	private TreeMap <String, Integer> makeGroceryListMaterials(){

		TreeMap <String, Integer> groceryListMaterials = new TreeMap <String, Integer>(); 
		Vector <AssetContent> assetContents = fAsset.getAssetContents();
		Iterator <AssetContent> assetContent = assetContents.iterator();

		while (assetContent.hasNext()){										//run over all contents in the house
			String nameOfAssetContent = assetContent.next().getName();
			List<RepairMaterialInformation> ListofRepairMaterialsNeeded = fRepairMaterialInformationDS.get(nameOfAssetContent);
			Iterator <RepairMaterialInformation> repairMaterial = ListofRepairMaterialsNeeded.iterator();

			while (repairMaterial.hasNext()){									//run over all repair materials needed to fix the specified tool
				RepairMaterialInformation currentRepairMaterial =  repairMaterial.next();
				if (groceryListMaterials.containsKey(currentRepairMaterial.getName())){
						groceryListMaterials.put(currentRepairMaterial.getName(), (currentRepairMaterial.getQuantity()+groceryListMaterials.get(currentRepairMaterial.getName()).intValue()));
				}else{
					groceryListMaterials.put(currentRepairMaterial.getName(), currentRepairMaterial.getQuantity());   
				}
			} 
		}
		LOGGER.finest("making Grocery List Material is done");
		return groceryListMaterials;
	}

}
