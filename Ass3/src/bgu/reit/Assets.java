package bgu.reit;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


class Assets {

	private final ArrayList <Asset> fAssetsDS = new ArrayList<Asset>(); 

	/**
	 * Constructor of Assets objects<P>
	 * the initialization of the @param fassetsDB do out of the constructor
	 */
	public Assets(){

	}


	/**
	 * this method will insert new object into the @param fassetsDB
	 * @param newAsset - the new Asset object to insert 
	 * @return true if the insertion was done
	 */
	public boolean insertAsset(Asset newAsset){
		if (newAsset!=null){
			fAssetsDS.add(newAsset);
			return true;
		}
		else return false;
	}


	/**
	 * this method will retrieve a suitable asset to the clerk demands
	 * @return Asset
	 */
	public synchronized Asset find(int size,String type){
		Asset suitableAsset = null;
		while (suitableAsset == null){
			for (Asset asset : fAssetsDS){
				if ((asset.getAssetType().equals(type)) && (asset.getAssetSize() >= size) && (asset.getStatus() == AssetStatus.AVAILABLE)){
					suitableAsset = asset;
					asset.updateStatusToBooked();
					return suitableAsset;
				}
			}
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}


	/**
	 * This method passes on all the assets and checks if it's damaged 
	 * @return List <Asset> - list of damaged assets
	 */
	public List <Asset> getDamagedAssets(){
		List <Asset> damagedAssets = new LinkedList<Asset>();
		for (Asset asset : fAssetsDS){
			if (asset.getHealthStatus()<65){
				damagedAssets.add(asset);
			}
		}
		return damagedAssets;
	}

}
