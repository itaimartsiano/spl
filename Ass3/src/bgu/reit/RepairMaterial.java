package bgu.reit;

import java.util.concurrent.atomic.AtomicInteger;

class RepairMaterial {

	private String fName;
	private AtomicInteger fQuantity;


	/**
	 * constructor for repair material
	 * @param name - name of the material
	 * @param quantity - quantity of the material
	 */
	public RepairMaterial(String name, int quantity){
		fName = name;
		fQuantity = new AtomicInteger(quantity);
	}


	/**
	 * getter for the name of the repair material
	 * @return String
	 */
	public String getName() {
		return fName;
	}
	
	
	/**
	 * This method updates the quantity of the tool 
	 * @param quantity - the quantity requested of the tool 
	 * @return true if succeeded in taking the amount from the tool
	 */
	public void acquire(int quantity){
		fQuantity.getAndAdd((-quantity));
	}

	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		 builder.append("RepairMaterial [");
		 builder.append("name= ");
		 builder.append(fName);
		 builder.append(", quantity= ");
		 builder.append(fQuantity).append("]").append("\n");
		return builder.toString();
	}
}
