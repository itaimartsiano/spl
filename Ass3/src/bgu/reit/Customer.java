package bgu.reit;

class Customer {

	private String fName;
	private String fVandalism; //have 3 option Arbitrary,Fixed,None
	private double fMinimumDamage;
	private double fMaximumDamage;


	/**
	 * constructor of a customer
	 * @param name - name of a customer
	 * @param vandalism - the type of vandalism the customer does
	 * @param minimumDamage - minimum damage the customer can do
	 * @param maximumDamage - maximum damage the customer can do
	 */
	public Customer(String name, String vandalism, double minimumDamage, double maximumDamage){
		fName = name;
		fVandalism = vandalism;
		fMinimumDamage = minimumDamage;
		fMaximumDamage = maximumDamage;

	}

	
	/**
	 * getter for the name
	 * @return String
	 */
	public String getfName() {
		return fName;
	}

	
	/**
	 * getter for the vandalism
	 * @return String
	 */
	public String getfVandalism() {
		return fVandalism;
	}


	/**
	 * getter for the minimum damage
	 * @return double
	 */
	public double getfMinimumDamage() {
		return fMinimumDamage;
	}


	/**
	 * getter for the maximum damage
	 * @return double
	 */
	public double getfMaximumDamage() {
		return fMaximumDamage;
	}


	/**
	 * calculate the damage according to the type of vandalism
	 * @return float, the damage percentage the customer does
	 */
	public float calculateDamage(){
		float damagePercentage = 0;
		if (fVandalism.equals("Arbitrary")){
			damagePercentage = (float) ((Math.random() * (fMaximumDamage - fMinimumDamage)) + fMinimumDamage);
		}else 
			if (fVandalism.equals("Fixed")){
				damagePercentage = (float) (fMaximumDamage + fMinimumDamage)/2;
			}else 
				if (fVandalism.equals("None")){
					damagePercentage = 0.5f;
				}
		return damagePercentage;
	}
}
