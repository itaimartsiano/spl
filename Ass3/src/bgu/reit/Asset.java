package bgu.reit;
import java.util.Vector;


class Asset {

	private final String fName;
	private final String fType;				
	private final Location fLocation;
	private final Vector <AssetContent> fAssetContent;
	private final double fCostPerNight;		
	private final int fSize;				
	private double fHealth; 
	private AssetStatus fStatus;					


	/**
	 * constructor of asset
	 * @param name - name of the asset
	 * @param type - type of the asset
	 * @param location - location of the asset
	 * @param assetContents - all the asset contents that are in the asset 
	 * @param costPerNight - cost of staying in asset per night
	 * @param size - size of the asset
	 */
	public Asset(String name, String type, Location location ,double costPerNight, int size){
		this.fName = name;
		this.fType = type;			
		this.fLocation = location;
		this.fAssetContent = new Vector<AssetContent>();
		this.fCostPerNight = costPerNight;		
		this.fSize = size;				
		this.fStatus = AssetStatus.AVAILABLE;
		this.fHealth = 100; 
	}


	/**
	 * getter for health of the asset
	 * @return double 
	 */
	public  double getHealthStatus() {
		return fHealth;
	}

	/**
	 * getter for type of the asset
	 * @return String
	 */
	public String getAssetType() {
		return fType;
	}

	/**
	 * getter for the status of the asset
	 * @return AssetStatus
	 */
	public AssetStatus getStatus() {
		return fStatus;
	}


	/**
	 * getter for size of the asset
	 * @return int
	 */
	public int getAssetSize() {
		return fSize;
	}

	/**
	 * getter for the asset contents of the asset
	 * @return Vector that contain all the asset contents
	 */
	public Vector <AssetContent> getAssetContents(){
		return fAssetContent;
	}


	/**
	 * getter for the asset name
	 * @return String-the name of the asset
	 */
	public String getName() {
		return fName;
	}

	
	/**
	 * getter for the asset location
	 * @return Location object-the location of the asset
	 */
	public Location getLocation() {
		return fLocation;
	}

	/**
	 * getter for the asset cost per night
	 * @return Location object-the location of the asset
	 */
	public double getCostPerNight() {
		return fCostPerNight;
	}

	/**
	 * This method adds an asset content to the vector of asset contents
	 * @param name - name of asset content
	 * @param repairCost - the cost to fix the asset content
	 */
	public void addAssetContent(String name , double repairCost){
		fAssetContent.add(new AssetContent(name,repairCost));
	}


	/**
	 * This method calculate how much time need to repair this asset
	 * @return double- time needed to repair this asset
	 */
	public double assetRepairCostTime(){
		double repairCost = 0;
		for(int i=0; i<fAssetContent.size();i++){
			repairCost = repairCost + fAssetContent.get(i).calculateRepairCost(fHealth);
		}
		return repairCost;
	}

	/**
	 * This method update the health of the asset after customers have stayed in it
	 * @param damagePercentage - how much damage is done to the asset
	 */
	public void updateHealthAfterStay(double damagePercentage){
		int MINIMUM_HEALTH_TO_STAY = 65; //, if the health percentage is under 65, update status to unavialable
		fHealth=fHealth-damagePercentage;
		if (fHealth < MINIMUM_HEALTH_TO_STAY) {
			updateStatusToUnavailable();
		}
	}

	
	/**
	 * This method update the health of the asset after the maintenance fixed it
	 */
	public  void updateHealthAfterFix(){
		fHealth=100;
		fStatus = AssetStatus.AVAILABLE;
	}


	/**
	 * This method update the status of the asset when the customers stay in the asset
	 */
	public void updateStatusToOccupied() {
		fStatus = AssetStatus.OCCUPIED;

	}


	/**
	 * This method update the status of the asset when the customers finished staying in the asset
	 */
	public void updateStatusToAvailable() {
		fStatus = AssetStatus.AVAILABLE;		
	}


	/**
	 * This method update the status of the asset when the asset needs to be fixed
	 */
	public void updateStatusToUnavailable() {
		fStatus = AssetStatus.UNAVIALABLE;		
	}


	/**
	 * This method update the status of the asset when the clerk assigned it to a rental request
	 */
	public void updateStatusToBooked() {
		fStatus = AssetStatus.BOOKED;
	}


	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(fName);
		return builder.toString();

	}

}
