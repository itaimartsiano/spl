package bgu.reit;

class RepairMaterialInformation {

	private final String fName;
	private final int fQuantity;
	
	
	/**
	 * constructor of the repair material information
	 * @param name - name of the repair material information
	 * @param quantity - quantity of the repair material information
	 */
	public RepairMaterialInformation(String name, int quantity){
		fName=name;
		fQuantity=quantity;
	}
	
	
	/**
	 * getter for the name of the repair material information
	 * @return String
	 */
	public String getName() {
		return fName;
	}

	
	/**
	 * getter for the quantity of the repair material information
	 * @return int
	 */
	public int getQuantity() {
		return fQuantity;
	}
	
}
