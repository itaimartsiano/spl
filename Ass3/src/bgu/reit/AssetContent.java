package bgu.reit;

class AssetContent {

	private String fName;
	private double fRepairCostMultiplier;

	/**
	 * constructor of an asset content in a given asset
	 * @param name - name of the asset content
	 * @param repairCostMultiplier - the cost to fix the asset content
	 */
	public AssetContent(String name,double repairCostMultiplier){
		fName=name;
		fRepairCostMultiplier=repairCostMultiplier;
	}


	/**
	 * getter for the name of the asset content
	 * @return String -name of asset content
	 */
	public String getName(){
		return fName;
	}


	/**
	 * This method calculate the repair cost of this asset contact
	 * @param health -the health of the asset is the health of the asset contact
	 * @return double - the time need to repair this asset contact
	 */
	public double calculateRepairCost(double health){
		return (100-health)*fRepairCostMultiplier;
	}
}
