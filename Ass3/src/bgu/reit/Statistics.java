package bgu.reit;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

class Statistics {
	
	private AtomicInteger fMoneyGained = new AtomicInteger();
	private Vector <RentalRequest> fHandledRequests = new Vector <RentalRequest>(); 
	private Map <String,AtomicInteger> fUsedrepairToolDS = new HashMap <String,AtomicInteger>();	
	private Map <String,AtomicInteger> fUsedrepairMaterialDS = new HashMap <String,AtomicInteger>();
	
	public Statistics(){
		
	}
	
	
	/**
	 * This method update the money gained field
	 * @param numberOfPeople - how many people stay in asset
	 * @param durationOfStay - how long the people stay in asset
	 * @param costPerNight - how much money cost to stay in asset , per night
	 */
	public void updateRentalProfit(int numberOfPeople , double durationOfStay , double costPerNight){
		int profit = (int) (numberOfPeople * durationOfStay * costPerNight);
		fMoneyGained.getAndAdd(profit);
	}
	
	
	/**
	 * This method update the rental request HashMap
	 * @param currentRentalRequest
	 */
	public void addRentalRequest(RentalRequest currentRentalRequest){
		fHandledRequests.add(currentRentalRequest);
	}
	
	
	/**
	 * This method update the RepairTool vector
	 * @param name - name of the tool
	 * @param quantity - quantity of the tool
	 */
	public synchronized void addRepairTool(String name , int quantity){
		if (fUsedrepairToolDS.containsKey(name)){
			fUsedrepairToolDS.get(name).addAndGet(quantity);
		}else{
			AtomicInteger atomicQuantity = new AtomicInteger ();
			atomicQuantity.addAndGet(quantity);
			fUsedrepairToolDS.put(name,atomicQuantity);
		}
	}
	
	
	/**
	 * This method update the RepairMaterial vector
	 * @param name - name of the material
	 * @param quantity - quantity of the material
	 */
	public synchronized void addRepairMaterial(String name , int quantity){
		if (fUsedrepairMaterialDS.containsKey(name)){
			fUsedrepairMaterialDS.get(name).addAndGet(quantity);
		}else{
			AtomicInteger atomicQuantity = new AtomicInteger ();
			atomicQuantity.addAndGet(quantity);
			fUsedrepairMaterialDS.put(name,atomicQuantity);
		}
	}
	
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("\n");
		builder.append(" The amount of money gained from rentals is: ");
		builder.append(fMoneyGained);
		builder.append("\n");
		builder.append("\n");
		builder.append("RentalRequests:").append("\n");
		
		for(int i=0; i<fHandledRequests.size();i++ ){
			builder.append(fHandledRequests.get(i).toString());
		}
		
		builder.append("\n");
		builder.append("Tools usage:").append("\n");
		for (Map.Entry<String,AtomicInteger> entry : fUsedrepairToolDS.entrySet()) {
			builder.append(entry.getValue()).append(" ");
			builder.append(entry.getKey()).append(" ,were used").append("\n");
		}
		
		builder.append("\n");
		builder.append("Materials usage:").append("\n");
		for (Map.Entry<String,AtomicInteger> entry : fUsedrepairMaterialDS.entrySet()) {
			builder.append("[").append(entry.getKey()).append(", ");
			builder.append(entry.getValue()).append("]").append("\n");
		}
		return builder.toString();
	}

}
