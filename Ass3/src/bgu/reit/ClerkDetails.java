package bgu.reit;

class ClerkDetails {

	private final String fName;
	private final Location fLocation;

	/**
	 * constructor of Clerks in Reits
	 * @param name  -Clerk's name
	 * @param location - Clerk's location
	 */
	public ClerkDetails(String name, Location location){
		this.fName = name;
		this.fLocation = location;
	}


	/**
	 *getter for the name of the clerk
	 *@return String
	 */
	public String getName(){
		return fName;
	}


	/**
	 *getter for the location of the clerk
	 *@return Location 
	 */
	public Location getLocation(){
		return fLocation;
	}
}
