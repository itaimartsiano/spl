package bgu.reit;

import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

class RunnableCustomerGroupManager implements Runnable {

	private CustomerGroupDetails fCustomerGroupDetails;
	private Management fManagement;
	private final Logger LOGGER = Logger.getLogger(RunnableCustomerGroupManager.class.getName());
	private Statistics fStatistics;
	
	/**
	 * Constructor to RunnableCustomerGroupManager
	 * @param customerGroupDetails 
	 * @param management
	 */
	public RunnableCustomerGroupManager(CustomerGroupDetails customerGroupDetails, Management management,Statistics statistics){
		fCustomerGroupDetails = customerGroupDetails;
		fManagement = management;
		fStatistics = statistics;
	}


	/**
	 * this is the run function of runnable object RunnableCustomerGroupManager <P>
	 * the managers take care of the customers
	 */
	public void run(){
		LOGGER.fine(Thread.currentThread().getName()+": "+fCustomerGroupDetails.getName()+" started working");
		while (fCustomerGroupDetails.getNumberOfRentalRequests()>0){
			RentalRequest currentRentalRequest = fCustomerGroupDetails.getRentalRequest();
			Asset currentAsset = null;
			synchronized(currentRentalRequest){
				fManagement.addRentalRequestToBlockingQueue(currentRentalRequest);
				LOGGER.info(fCustomerGroupDetails.getName()+" submitted rental request: "+currentRentalRequest.getId() );
				try {
					LOGGER.info(fCustomerGroupDetails.getName()+" waiting until the rental request fulfilled");
					currentRentalRequest.wait();
					currentAsset = currentRentalRequest.getAsset();
				} catch (InterruptedException exception) {
					LOGGER.severe("RunnableCustomerGroupManager was interupted while waiting on the rental request");
					exception.printStackTrace();
				}
			}
			float damagePrecentage;
			synchronized (currentAsset){
				currentAsset.updateStatusToOccupied();
				currentRentalRequest.updateRequestStatusToInProgress();
				fStatistics.addRentalRequest(currentRentalRequest);
				fStatistics.updateRentalProfit(fCustomerGroupDetails.getContainerOfCustomers().size(), currentRentalRequest.getfDurationOfStay(), currentAsset.getCostPerNight()); //updates statistics
				LOGGER.info(fCustomerGroupDetails.getName()+" started simulation staying in : "+currentAsset.getName());
				damagePrecentage = stayInHouse(currentRentalRequest.getfDurationOfStay()); 
				currentAsset.updateStatusToAvailable();
				currentRentalRequest.updateRequestStatusToComplete();
			}
			LOGGER.info("notify all clerks , "+ currentAsset.getName()+" vacated" );
			fManagement.wakeUpSleepOnAssets();
			fManagement.addDamageReport(new DamageReport(currentAsset, damagePrecentage));
			LOGGER.info(fCustomerGroupDetails.getName()+" a dmamage report had been submitted");
		}

	}


	/**
	 * This method simulate the customer staying in the asset
	 * @param durationOfStay
	 * @return float ,the damage percentage
	 */
	private float stayInHouse(double durationOfStay) {	
		Vector <Customer> containerOfCustomers = fCustomerGroupDetails.getContainerOfCustomers();
		ExecutorService executorStayInHouse = Executors.newFixedThreadPool(fCustomerGroupDetails.getContainerOfCustomers().size());
		CompletionService <Float> stayInHouseCompletionService = new ExecutorCompletionService<Float>(executorStayInHouse);

		for (Customer customer : containerOfCustomers)
		{
			Callable<Float> customerStayinAsset = new CallableSimulateStayInAsset(customer,durationOfStay);
			stayInHouseCompletionService.submit(customerStayinAsset);
		}

		float damagePercentage = 0;
		for (int i = 0; i < containerOfCustomers.size(); i++){
			try {
				damagePercentage += stayInHouseCompletionService.take().get();
			} catch (InterruptedException | ExecutionException exception) {
				LOGGER.severe("stay in house simulation was interupted");
				exception.printStackTrace();
			}
		}
		executorStayInHouse.shutdown();
		LOGGER.info(fCustomerGroupDetails.getName()+" ended the simulation stay in asset ");
		return damagePercentage;
	}
	
}
