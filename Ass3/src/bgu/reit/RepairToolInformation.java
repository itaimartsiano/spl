package bgu.reit;

class RepairToolInformation {
	
	private final String fName;
	private final int fQuantity;
	
	
	/**
	 * constructor of the repair tool information
	 * @param name - name of the repair tool information
	 * @param quantity - quantity of the repair tool information
	 */
	public RepairToolInformation(String name, int quantity){
		fName=name;
		fQuantity=quantity;
	}

	
	/**
	 * getter for the name of the repair tool information
	 * @return String
	 */
	public String getName() {
		return fName;
	}
	
	
	/**
	 * getter for the quantity of the repair tool information
	 * @return int
	 */
	public int getQuantity(){
		return fQuantity;
	}

}
