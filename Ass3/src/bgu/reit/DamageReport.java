package bgu.reit;


class DamageReport {

	private final Asset fDamagedAsset;
	private final double fDamagePrecentage; 


	/**
	 * Constructor to DamageReport, initialize two parameters fields
	 * @param damagedAsset - the reported Asset
	 * @param damagePrecentage - the percentage of damage in the asset
	 */
	public DamageReport(Asset damagedAsset, double damagePrecentage){
		this.fDamagedAsset = damagedAsset;
		this.fDamagePrecentage = damagePrecentage;
	}


	/**
	 * getter for the damage of the asset
	 * @return double
	 */
	public double getAssetDamage() {
		return fDamagePrecentage;

	}


	/**
	 * getter for the asset that this damage report was created for
	 * @return Asset
	 */
	public Asset getAsset() {
		return fDamagedAsset;
	}

}
