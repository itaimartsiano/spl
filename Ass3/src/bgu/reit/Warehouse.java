package bgu.reit;

import java.util.HashMap;
import java.util.Map;



class Warehouse {
	
	private Map <String,RepairTool> fRepairToolDataStructure;
	private Map <String,RepairMaterial> fRepairMaterialDataStructure;

	
	/**
	 * constructor to Warehouse <p> 
	 */
	public Warehouse(){
		fRepairMaterialDataStructure = new HashMap <String,RepairMaterial>();
		fRepairToolDataStructure = new HashMap <String,RepairTool>();
	}
	
	
	/**
	 * setter to Warehouse - adding ReapirTool <p>
	 * @param repairtool - the RepairTool object to insert into the warehouse
	 * @return true if succeeded to insert the object
	 */
	public boolean addRepairTool(RepairTool repairtool){
		fRepairToolDataStructure.put(repairtool.getName(), repairtool);
		return true;
	}
	
	/**
	 * setter to Warehouse - adding ReapirMaterial<p>
	 * @param ReapirMaterial - the ReapirMaterial object to insert into the warehouse
	 * @return true if succeeded to insert the object
	 */
	public boolean addRepairMaterial(RepairMaterial repairtool){
		fRepairMaterialDataStructure.put(repairtool.getName(), repairtool);
		return true;
	}
	
	/**
	 * This method acquire tool from the warehouse
	 * @return true if succeeded
	 */
	public boolean acquireRepairTool(String name, int quantity) {
		RepairTool currenRepairTool = fRepairToolDataStructure.get(name);
		return currenRepairTool.acquire(quantity);
	}

	/**
	 * This method acquire material from the warehouse
	 * @return true if succeeded
	 */
	public boolean acquireRepairMaterial(String name, int quantity) {
		RepairMaterial currenRepairMaterial = fRepairMaterialDataStructure.get(name);
		currenRepairMaterial.acquire(quantity);
		return true;
	}

	/**
	 * This method release tools 
	 * @return true if succeeded
	 */
	public boolean releasRepairTool(String name, int quantity) {
		RepairTool currenRepairTool = fRepairToolDataStructure.get(name);
		return currenRepairTool.release(quantity);
	}

}
