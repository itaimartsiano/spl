package bgu.reit;

import java.util.concurrent.Semaphore;


class RepairTool {
	private String fName;
	private Semaphore fQuantity;


	/**
	 * constructor for repair tool
	 * @param name - name of the tool
	 * @param quantity - quantity of the tool
	 */
	public RepairTool(String name, int quantity){
		fName = name;
		fQuantity = new Semaphore(quantity);
	}

	
	/**
	 * getter for the name of the repair tool
	 * @return String
	 */
	public String getName() {
		return fName;
	}

	
	/**
	 * This method updates the quantity of the tool 
	 * @param quantity - the quantity requested of the tool 
	 * @return true if succeeded in taking the amount from the tool
	 */
	public boolean acquire(int quantity){
			return fQuantity.tryAcquire(quantity);
	}

	
	/**
	 * This method updates the quantity of the tool 
	 * @param quantity - the quantity requested of the tool 
	 * @return true if succeeded in releasing the amount from the tool
	 */
	public boolean release(int quantity){
			fQuantity.release(quantity);
			return true;
	}

	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		 builder.append("RepairTool [");
		 builder.append("name= ");
		 builder.append(fName);
		 builder.append(", quantity= ");
		 builder.append(fQuantity).append("]").append("\n");
		return builder.toString();
	}
}
