package bgu.reit;

import java.util.HashMap;


public class WarehouseTesting extends Warehouse {

	HashMap<String, RepairTool> collectionRepairTool;
	HashMap<String, RepairMaterial> collectionRepairMaterial;
	
	public WarehouseTesting(){
		collectionRepairMaterial = new HashMap<String,RepairMaterial>();
		collectionRepairTool = new HashMap<String,RepairTool>();
	}
	
	public void addRepairTool (String name, RepairTool tool){
		this.collectionRepairTool.put(name, tool);
	}
	
	public void addRepairMaterial (String name, RepairMaterial material){
		this.collectionRepairMaterial.put(name, material);
	}
}
