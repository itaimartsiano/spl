package bgu.reit;

class RentalRequest {
	private String fId;
	private String fAssetType;
	private int fAssetSize;
	private double fDurationOfStay;
	private Asset fAsset;
	private RequestStatus fRequestStatus; //one of these options : Incomplete,Fulfilled,InProgress,Complete


	/**
	 * constructor for rental request
	 * @param id - id of the rental request
	 * @param assetType - the asset type of the rental request
	 * @param assetSize - the size type of the rental request
	 * @param durationOfStay - the duration of stay in the asset 
	 * @param asset - the asset requested
	 */
	public RentalRequest(String id,String assetType,int assetSize,double durationOfStay,Asset asset){
		fId=id;
		fAssetType=assetType;
		fAssetSize=assetSize;
		fDurationOfStay=durationOfStay;
		fAsset=asset;
		setfRequestStatus(RequestStatus.Incomplete);
	}

	
	/**
	 * getter for the asset
	 * @return Asset
	 */
	public Asset getAsset() {
		return fAsset;
	}
	
	
	/**
	 * getter for the id
	 * @return String
	 */
	public String getId() {
		return fId;
	}


	/**
	 * getter for the asset size
	 * @return int
	 */
	public int getAssetSize() {
		return fAssetSize;
	}


	/**
	 * getter for the asset type
	 * @return String
	 */
	public String getAssetType() {
		return fAssetType;
	}
	

	/**
	 * getter for the duration of the customers in the asset
	 * @return double
	 */
	public double getfDurationOfStay() {
		return fDurationOfStay;
	}
	
	
	/**
	 * getter for the the status of the rental request
	 * @return RequestStatus
	 */
	public RequestStatus getfRequestStatus() {
		return fRequestStatus;
	}

	
	/**
	 * setter for the the status of the rental request
	 * @param fRequestStatus - the new status
	 */
	private void setfRequestStatus(RequestStatus fRequestStatus) {
		this.fRequestStatus = fRequestStatus;
	}

	
	/**
	 * setter for the asset
	 * @param asset
	 */
	public void setAsset(Asset asset){
		fAsset = asset;
	}


	/**
	 * This method update the status of the rental request to fulfilled
	 */
	public void updateRequestStatusToFulfilled(){
		setfRequestStatus(RequestStatus.Fulfilled);
	}

	
	/**
	 * This method update the status of the rental request to InProgress
	 */
	public void updateRequestStatusToInProgress(){
		setfRequestStatus(RequestStatus.InProgress);
	}

	
	/**
	 * This method update the status of the rental request to Complete
	 */
	public void updateRequestStatusToComplete(){
		setfRequestStatus(RequestStatus.Complete);
	}

	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		 builder.append("[ id: ");
		 builder.append(fId).append(", ");
		 builder.append("Asset  assigned: ");
		 builder.append(fAsset.toString()).append("]").append("\n");
		 
		return builder.toString();
	}

}
