package spl.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StackImplTest {
	
	BoundedStack<Integer> bounded_stack;
	
	@Before
	public void setUp() throws Exception {
		this.bounded_stack = new BoundedStack<Integer>(4);
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	/**
	 * 
	 */
	public void testStackimpl() {
		 assertNotNull("new Stack is not null", this.bounded_stack);
		 assertTrue("new Stack is not empty", this.bounded_stack.isEmpty());
	}

	@Test
	/**
	 * this method should test push objects into stack
	 * 
	 * test push objects to non full stack and check if the size get bigger
	 * test push object to full stack and check if it throw exceptions
	 * try to push null object
	 */
	public void testPush() throws Exception {
		try{
			bounded_stack.push(new Integer (1));
			bounded_stack.push(new Integer (1));
			bounded_stack.push(new Integer (1)); 
			assertEquals(1,bounded_stack.freememory());
			bounded_stack.push(new Integer (1));
			assertEquals(0, bounded_stack.freememory()); 
			bounded_stack.push(new Integer (1));
			fail("stack is full");
		}
		catch(Exception t){
			
		}
	}

	@Test
	/**
	 * this method suppose to delete the first object in the stack and return it to user
	 * test if pop return object when the stack is not empty
	 * test if the program throw exceptions when trying to pop out when the stack is empty 
	 */
	
	public void testPop() throws Exception {
		bounded_stack.push(1); 
		bounded_stack.push(2);
		assertEquals((bounded_stack.freememory()),2);
		assertEquals(bounded_stack.pop().intValue(),2);
		bounded_stack.pop();
		//bounded_stack.pop();
		//fail("the stack is empty");
		
	}

	@Test
	/**
	 * check if it returns false on not empty stack
	 * check if it return true on empty stack
	 */
	public void testIsEmpty() throws Exception {
		bounded_stack.push(1); 
		bounded_stack.push(1);
		assertFalse(bounded_stack.isEmpty());
		bounded_stack.pop();
		bounded_stack.pop();
		assertTrue(bounded_stack.isEmpty());
	}
	}

