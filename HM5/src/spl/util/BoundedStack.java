package spl.util;


import java.util.LinkedList;

public class BoundedStack<T> implements Stack<T> {
	
	private LinkedList<T> stack;
	private int size;
	
	public BoundedStack(int currsize){
		this.stack = new LinkedList<T>();
		size=currsize;
	}
	
	@Override
	public void push(T obj) throws Exception {
		if (stack.size() <size){
			stack.addFirst(obj);
		}else throw new Exception ("the stack is full");

	}

	@Override
	public T top() throws Exception {
		if (stack.size()>0){
			return stack.getFirst();
		}else{
			throw new Exception ("the stack is full");
		}
		
	}

	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}

	@Override
	public T pop() throws Exception {
		if (stack.size()>0){
			T ans = stack.getFirst();
			stack.removeFirst();
			return ans;
		}else throw new Exception ("the stack is empty");
	}

	@Override
	public int freememory() {
		return (this.size)-stack.size();
	}

}
