package spl.util;

public interface Stack<T> {
    /**
     * add the object at the top of the stack. (This is a command.)
     * 
     * @param obj
     *            any non null T object.
     * @throws Exception 
     * @pre: none.
     * @post: isEmpty() is false.
     * @post: count() == @pre(count()) + 1
     * @post: top() == @param obj
     */
    void push(T obj) throws Exception;
 
    
    /**
     * remove the top object from the stack. (This is a command.)
     * 
     * @throws Exception
     * @pre: isEmpty() == false
     * @post: count() == @pre(count()) - 1
     */
    T pop() throws Exception;
 
    /**
     * @return the value of the top object on the stack. (This is a query.)
     * 
     * @throws Exception
     * @pre: this.isEmpty() == false
     */
    T top() throws Exception;
 
    /**
     * @return True if the Stack is empty, or False if the Stack contains at
     *         least one {@link Object}. (This is a query.)
     * @post: @return (count()==0)
     */
    boolean isEmpty();
    int freememory();
 
        
}
