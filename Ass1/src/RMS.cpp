#include "../include/RMS.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <cstdlib>

using namespace std;

//computing the stress facor and make stress factor file
void RoadStressC (){
  
  ifstream roads ("Roads.conf");
  ifstream routes ("Routes.conf");
  string line;
  vector <string> road;				//vector of all the roads
  map <string, int> length;			//map of road's length
  map <string, int> Nroutes;			//map of number of routes per road
  
  while (!roads.eof()){				 
    
    getline(roads,line);			//insert each road to the vector(roads) (get the roads from Roads.conf) 
    if (line.length()>3){
      int found = line.find_last_of(','); 
      string junction=line.substr(0,(found));
      road.push_back(junction);
      
      Nroutes[junction] = 0;			//give starting value of routes for each road to Nroutes map
      
      string strlval = line.substr(found+1);	//insert length for each road to length map
      int lval = atoi(strlval.c_str());
      length[junction] = lval; 
      line="";
    }
  }  
  
  roads.close();
 
  unsigned int beg, mid;
  int sof;
  
  while (!routes.eof()){			//counting number of routes for each road
      
    getline(routes,line);
  
    while(mid != line.length()){
      
      mid = line.find(',',(beg));
      sof = line.find(',',(mid+1));
      if (sof==-1) sof = line.length();
      string tmp = line.substr(beg,(sof-beg));
      Nroutes[tmp] = (Nroutes.at(tmp)+1);
      beg = mid+1;
      mid = sof;  
      
    }
    
    beg =0;
    mid =0;
    sof=0;
    
  }   

  routes.close();
   
  ofstream myfile;	
  myfile.open ("RoadStress.out");		//creating the output file
  unsigned int i;
  
  for (i=0; i<road.size(); i++){		//insert the stress factor to the output file
    
    string roadname = road.at(i);
    double strfactor =((Nroutes.at(roadname))/(double)(length.at(roadname)));
    myfile << roadname<< "," << strfactor << '\n';
    
  }
  
  myfile.close();
      
}



  